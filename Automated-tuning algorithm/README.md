## Automated-tuning algorithm 

#### Folder:
- PMMSGD: contains the python scripts to run the "Poor Man's Majorana Stochastic Gradient Descent" (PMMSGD) algorithm including the CNN network, 
the pre-trained network parameter, and the re-training script on experimental data ("CNN_28_offline_retraining.ipynb")
- plotting scripts: contains plotting scripts of the publication, including the training and re-training of the CNN with the simulated and experimental data


#### "setup.py" and "requirements.txt" can be used to implement the code on an experimental device to use the automated-tuning algorithm 