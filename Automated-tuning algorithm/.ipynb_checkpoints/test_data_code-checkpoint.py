import numpy as np
import os
import xarray
def closest_index(lst, val):
    """
    Returns a random index of the value(s) in the list that are closest to the given value.
    """
    min_dist = float('inf')
    indices = []
    for i, item in enumerate(lst):
        dist = abs(item - val)
        if dist < min_dist:
            min_dist = dist
            indices = [i]
        elif dist == min_dist:
            indices.append(i)
    return indices[0]

def remove_duplicates_get_indices(lst):
    """
    Remove duplicates from a list and return their indices.

    Parameters:
        lst (list): The list to remove duplicates from.

    Returns:
        list: The indices of the removed duplicates.
    """
    unique = []
    indices = []
    for i, x in enumerate(lst):
        if x not in unique:
            unique.append(x)
        else:
            indices.append(i)
    return indices

def load_datasets(directory):
    """
    Load datasets from a directory, retrieve V_ABS and Delta_minus_t attributes.

    Parameters:
        directory (str): Path to the directory containing datasets.

    Returns:
        tuple: A tuple containing lists of datasets, V_ABSs, and Delta_minus_ts.
    """
    all_dbx = []
    all_V_ABSs = []
    all_Delta_minus_t = []

    for file in os.listdir(directory):
        dbx = xarray.load_dataset(os.path.join(directory, file))
        all_dbx.append(dbx)
        param_tD = - getattr(dbx, 'Delta_minus_t') * 10000 * 2
        all_Delta_minus_t.append(param_tD)
        V_ABS = getattr(dbx, 'V_ABS')
        all_V_ABSs.append(V_ABS)

    return all_dbx, all_V_ABSs, all_Delta_minus_t

def filter_duplicates(all_dbx, all_V_ABSs, all_Delta_minus_t):
    """
    Remove duplicates based on all_V_ABSs and return filtered lists.

    Parameters:
        all_dbx (list): List of datasets.
        all_V_ABSs (list): List of V_ABS attributes.
        all_Delta_minus_t (list): List of Delta_minus_t attributes.

    Returns:
        tuple: Filtered lists of datasets, V_ABSs, and Delta_minus_ts.
    """
    dup_idx = remove_duplicates_get_indices(all_V_ABSs)
    
    all_dbx = [x for i, x in enumerate(all_dbx) if i not in dup_idx]
    all_Delta_minus_t = [x for i, x in enumerate(all_Delta_minus_t) if i not in dup_idx]
    all_V_ABSs = [x for i, x in enumerate(all_V_ABSs) if i not in dup_idx]
    
    return all_dbx, all_V_ABSs, all_Delta_minus_t

def load_test_data():
    exp_labels = np.zeros(1)
    exp_labels[0] = 1

    # Load datasets
    directory = '../../labelled_data/'
    all_dbx, all_V_ABSs, all_Delta_minus_t = load_datasets(directory)

    # Filter out duplicates
    all_dbx, all_V_ABSs, all_Delta_minus_t = filter_duplicates(all_dbx, all_V_ABSs, all_Delta_minus_t)
    return all_dbx, all_V_ABSs, all_Delta_minus_t