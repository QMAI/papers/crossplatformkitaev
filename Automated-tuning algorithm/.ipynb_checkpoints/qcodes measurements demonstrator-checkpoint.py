# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
# %load_ext autoreload
# %autoreload 2
import random
import os
import xarray
import numpy as np
import matplotlib.pyplot as plt
import proplot as pplt
import PMMSGD
from qcodes import Parameter, validators

class V_ABS_parameter(Parameter):
    def __init__(self, name, V_ABS_min, V_ABS_max):
        if V_ABS_min != None:
            vals = validators.Numbers(min_value=V_ABS_min, max_value=V_ABS_max)
            super().__init__(name=name, vals=vals)
        else:
            super().__init__(name=name)
        self.V_ABS = 0
    def get_raw(self):
        return self.V_ABS
    
    def set_raw(self, V_ABS):
        self.V_ABS = V_ABS


# -

# # Test function
# Mimics real measurement, uses cGAN to generate data

from skimage.transform import resize
def return_dbx():
    param = .5*(1e3*(V_ABSs_range[0]-V_ABS_dc())/V_ABSs_range.min()-.75)
    # GLL, DmtL = PMMSGD.cgan.generate_G_return(param+np.random.normal()*.2e-1, PMMSGD.scaler, PMMSGD.scaler_1, 1)
    # GRR, DmtL = PMMSGD.cgan.generate_G_return(param+np.random.normal()*.2e-1, PMMSGD.scaler, PMMSGD.scaler_1, 1)

    GLL, DmtL = PMMSGD.cgan.generate_G_return(param, PMMSGD.scaler, PMMSGD.scaler_1, 1)
    GRR, DmtL = PMMSGD.cgan.generate_G_return(param, PMMSGD.scaler, PMMSGD.scaler_1, 1)
    
    GLL -= GLL.min()
    GRR -= GRR.min()
    
    # GLL = resize(GLL, (51, 51))
    # GRR = resize(GRR, (51, 51))
    
    # GLL_enlarged, GRR_enlarged = np.zeros((51, 51)), np.zeros((51, 51))
    # start_row = (GLL_enlarged.shape[0] - GLL.shape[0]) // 2
    # start_col = (GLL_enlarged.shape[1] - GLL.shape[1]) // 2
    # GLL_enlarged[start_row:start_row+GLL.shape[0], start_col:start_col+GLL.shape[1]] = GLL
    # GRR_enlarged[start_row:start_row+GLL.shape[0], start_col:start_col+GLL.shape[1]] = GRR

    
    dims = ('QD1_plunger_fine_Vdc', 'QD2_plunger_fine_Vdc')
    coords = {'QD1_plunger_fine_Vdc': np.arange(GLL.shape[0]), 'QD2_plunger_fine_Vdc': np.arange(GLL.shape[1])}

    # Create data variables
    lockinLL_G = xarray.DataArray(GLL, coords=coords, dims=dims, name='lockinLL_G')
    lockinRR_G = xarray.DataArray(GRR, coords=coords, dims=dims, name='lockinRR_G')

    # Create the Dataset
    ds = xarray.Dataset({
        'lockinLL_G': lockinLL_G,
        'lockinRR_G': lockinRR_G
    })
    return ds


# # Test run

# + tags=[]
i_start = random.choice(range(len(PMMSGD.all_dbx)))
dbx_start = PMMSGD.all_dbx[i_start]
V_ABS_start  = PMMSGD.all_V_ABSs[i_start]
params = dict(
    dbx_start=dbx_start, 
    V_ABS_start=V_ABS_start,
    learning_rate = 1e-7, #1e-7 is good
    epochs = 50,
    min_step_size = 60e-6, #60 uV
    tolerance =.045,
    N = 5,  # Use the past 5 data points, for example,
    momentum = .5e-1,
    break_condition='minimum',
    V_ABS_min=-0.62431,
    V_ABS_max=-.62,
    verbose=True,
    classifier = 'CNN_differences',
    target=['lockinLL_G'],
    measure_func = return_dbx,
    plot=False
    
    
    
)
# V_ABS_dc = V_ABS_parameter("V_ABS", params['V_ABS_min'], params['V_ABS_max'])
V_ABS_dc = V_ABS_parameter("V_ABS", None, None)
V_ABSs_range = np.linspace(-.622, -.62)
normalized_range = .5*(1e3*(V_ABSs_range[0]-V_ABSs_range)/V_ABSs_range.min()-.75)
V_ABS_start = -.624
V_ABS_dc(V_ABS_start)
params['dbx_start'] = return_dbx()
params['V_ABS_start'] = V_ABS_start
params['V_ABS_parameter'] = V_ABS_dc
V_ABSs, obj_fun_values, final_objective, epoch = PMMSGD.SGD(**params)

# +
fig = pplt.figure(refwidth='70mm', refheight = '55mm', sharex=True, sharey=False)
ax = fig.subplots(nrows=1, ncols=2)
s=4
ax[0].plot(np.arange(len(V_ABSs)), obj_fun_values, marker = '.', markersize=s)
ax[0].format(xlabel='iteration', ylabel='t-Delta')
ax[0].axhline(params['tolerance'], c='red')

ax[1].plot(np.arange(len(V_ABSs)), V_ABSs, marker = '.', markersize=s)
ax[1].axhline(-.622, c='red')
ax[1].axhline(params['V_ABS_min'], c='k')
ax[1].axhline(params['V_ABS_max'], c='k')
ax[1].format(ylabel='V_ABS')

# ax[2].plot(np.array([PMMSGD.get_Delta_minus_t(V_ABS) for V_ABS in V_ABSs]), marker = '.', markersize=s)
# ax[2].axhline(0, c='red')
# ax[2].axhline(params['tolerance'], c='blue')
# ax[2].axhline(-params['tolerance'], c='blue')
# -

plt.scatter(V_ABSs, obj_fun_values)

PMMSGD.all_Delta_minus_t[0]

# +
if type(target) != list:
    target = [target]
if type(dbx) == xarray.Dataset: #only one entry
    total_data_array = []
    for t in target:
        target_array = get_dbx_target(dbx, t)
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_array) #could still change centering
        centered_target_array = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y)
        normalised_target_array = (target_array / np.amax(abs(target_array)) ) * 0.64 
        total_data_array.append(normalised_target_array)
  
    if len(target)==2:
        cnn_model.fit(total_data_array, labels, epochs=epochs, batch_size=batch_size,validation_split=validation_split)
    else:
        cnn_model.fit(2*total_data_array, labels, epochs=epochs, batch_size=batch_size,validation_split=validation_split)
else:
    dbx_list = dbx.copy()
    total_data_array = []
    for t in target:
        t_array = []
        for dbx in dbx_list:
            target_array = get_dbx_target(dbx, t)
            if center_x == None or center_y == None:
                center_x, center_y = find_center_hyperbolae(target_array) #could still change centering
            centered_target_array = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y)
            normalised_target_array = (target_array / np.amax(abs(target_array)) ) * 0.64 
            t_array.append(normalised_target_array)
        total_data_array.append(t_array)
  
    if len(target)==2:
        cnn_model.fit(total_data_array, labels, epochs=epochs, batch_size=batch_size,validation_split=validation_split)
    else:
        cnn_model.fit(2*total_data_array, labels, epochs=epochs, batch_size=batch_size,validation_split=validation_split)
    
    
    
# -



# +
# Combine the two lists using zip
combined = list(zip(keys, values))

# Sort combined list based on keys
sorted_combined = sorted(combined, key=lambda x: x[0])

# Unzip the sorted list
sorted_keys, sorted_values = zip(*sorted_combined)

print(sorted_keys)   # Outputs: (1, 2, 3)
print(sorted_values) # Outputs: ('one', 'two', 'three')

# -

if type(dbx) == xarray.Dataset: #only one entry
        if type(target) != list:
            target = [target]
        total_data_array = []
        for t in target:
            cut_image_scale = cut_and_scale_data(dbx, t, center_x = None, center_y = None)
            total_data_array.append(cut_image_scale)
        total_data_array = np.array(total_data_array)
        for L, t in zip(total_data_array, target): #this loops over all the targets
            print(f"training on {t}")
            cgan.train(L, labels, epochs=epochs, batch_size=batch_size)
            
    else:
        dbx_list = dbx.copy()
        total_data_array = []
        if type(target) != list:
            target = [target]
        target_array = []
        for t in target:
            target_array = []
            for dbx in dbx_list:
                cut_image_scale = cut_and_scale_data(dbx, t, center_x = None, center_y = None)
                target_array.append(cut_image_scale)
            total_data_array.append(np.array(target_array))
        total_data_array= np.array(total_data_array)
        print(total_data_array.shape)
        for L, t in zip(total_data_array, target): #this loops over all the targets
            print(f"training on {t}")
            cgan.train(L, labels, epochs=epochs, batch_size=batch_size)

dbx_list = dbx.copy()
for dbx in dbx_list:
    if type(target) == list:
        target_arrays = [get_dbx_target(dbx, t) for t in target] #these are the gLL, gRR, etc.
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_arrays[0]) #could still change centering

        centered_target_arrays = [cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y) for target_array in target_arrays]
        normalised_target_arrays = [(target_array / np.amax(abs(target_array)) ) * 0.64 for target_array in centered_target_arrays]
        if len(target)==2:
            cnn_model.fit(normalised_target_arrays, labels, epochs=epochs, batch_size=batch_size,validation_split=validation_split)
        else:
            cnn_model.fit(2*normalised_target_arrays, labels, epochs=epochs, batch_size=batch_size,validation_split=validation_split)
    else:
        dbx_list = dbx.copy()
        total_data_array = []
        target_arrays = [get_dbx_target(dbx, target)] #these are the gLL, gRR, etc.
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_arrays[0]) #could still change centering

        centered_target_arrays = [cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y) for target_array in target_arrays]
        normalised_target_arrays = [(target_array / np.amax(abs(target_array)) ) * 0.64 for target_array in centered_target_arrays]  

        cnn_model.fit(2*[normalised_target_arrays], _make_list_or_not(labels), epochs=epochs, batch_size=batch_size,validation_split=validation_split)


plt.plot(np.sort(PMMSGD.all_V_ABSs))


def _make_list_or_not(A):
    if type(A) == list:
        return A
    else:
        return [A]


type(PMMSGD.all_dbx[5]) == xarray.Dataset


def cut_and_scale_data(dbx, target, center_x = None, center_y = None):
    target_array = get_dbx_target(dbx, target)
    if center_x == None or center_y == None:
        center_x, center_y = find_center_hyperbolae(target_array)
    cut_image = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y, plot_images=False)
    cut_image_scale = cgan.scale_exp_data(cut_image) 
    return cut_image_scale


type(PMMSGD.all_dbx[5])

PMMSGD.retrain_CGAN(PMMSGD.cgan, 
                    PMMSGD.all_dbx[:5], 
                    ['lockinLL_G', 'lockinRR_G'], 
                    PMMSGD.all_Delta_minus_t[:5], 
                    x_points=25, center_x = None, center_y = None, epochs=5, batch_size=4)

# + tags=[]
PMMSGD.retrain_CNN(PMMSGD.cnn_model,
                   PMMSGD.cgan,
                PMMSGD.all_dbx[:5], 
                ['lockinLL_G'], 
                PMMSGD.all_Delta_minus_t[:5], 
                center_x = None, 
                center_y = None, 
                epochs=5, 
                batch_size=32, 
                validation_split=0.1)

# + tags=[]
# %debug
# -

# # Statistics

# + tags=[]
params['verbose'] = False
avgs, stds = [], []
learn_rates = np.logspace(-9, -5, 5)
for params['learning_rate'] in learn_rates:
    M=10
    epoch_ls =[]
    for _ in range(M):
        V_ABSs, obj_fun_values, final_objective, epoch = PMMSGD.SGD(**params)
        epoch_ls.append(epoch)
    epoch_avg = np.mean(epoch_ls)
    avgs.append(epoch_avg)
    stds.append(np.std(epoch_ls))
    
# -

plt.plot(learn_rates[:11], avgs)
plt.xscale('log')


# # Retrain

# +
def get_prediction_CGAN(cgan, dbx, target, x_points=25, center_x = None, center_y = None):
    """
    If you don't specify centers, it uses Alberto's centering algo
    
    Parameters:
    - cgan: trained CGAN model
    - x_points: number of different evaluations of the discriminator to obtain the rating function
        
    Returns:
    - t-Delta prediction of the CGAN averaged over gLL and gRR individually
    
    """
    if type(target) != list:
        target = [target]
    
    tD_cGAN_LL = 0
    for t in target:
        target_array = get_dbx_target(dbx, t)
        
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_array)
        cut_image = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y, plot_images=False)
        cut_image_scale = cgan.scale_exp_data(cut_image) 
        tD_cGAN = cgan.return_Delta_t(spectra=cut_image_scale, label=np.zeros((4,1)), scaler_1=scaler_1, n_plot=0, plot_orig = False, x_points=x_points)
        tD_cGAN += (tD_cGAN_LL - 0.35 ) * 4.5
    pred = tD_cGAN/len(target)
    return pred

def retrain_CGAN(cgan, dbx, target, labels, x_points=25, center_x = None, center_y = None, epochs=5, batch_size=4):
    """
    (Re-) training of the cGAN with new input data and labels.
    
    Parameters:
    - cgan: trained CGAN model
    - data: normalized [0,1] differential conductance data (28x28 pixels), array dimension: [#samples, 28, 28, 1].
            cGAN is trained individually on GLL and (or) GRR                                                                                
    - labels: normalized [0,1] t-Delta ratio in form of a 1D array, array dimension: [#samples, 1]
    - epochs: number of training iterations through full data set
    - batch_size: number of samples given to the cGAN at the same time    
    """
    if type(target) != list:
        target = [target]
    
    tD_cGAN_LL = 0
    for t in target:
        target_array = get_dbx_target(dbx, t)
        
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_array)
        cut_image = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y, plot_images=False)
        cut_image_scale = cgan.scale_exp_data(cut_image) 
        # tD_cGAN = cgan.return_Delta_t(spectra=cut_image_scale, label=np.zeros((4,1)), scaler_1=scaler_1, n_plot=0, plot_orig = False, x_points=x_points)
        
        cgan.train(cut_image_scale, labels, epochs=epochs, batch_size=batch_size)
       
    return None


# + [markdown] tags=[]
# # Experimental code

# +
def measure_avoided_crossing(QD1_center, QD1_range, QD2_center, QD2_range, delay = .25, N=28, centering=False, plot=False, lorentzian=False):
    run_id = PM3.measurements.sweep2d(dev, 'QD2_plunger_fine', QD2_center-QD2_range/2, QD2_center+QD2_range/2, N, delay,
                  'QD1_plunger_fine', QD1_center-QD1_range/2, QD1_center+QD1_range/2, N, delay,
                  dev.iL, dev.iR, dev.gLL, dev.gRR,
                  show_progress=True, use_threads=True)
    dbx = run_id[0].to_xarray_dataset()
    if centering:
        QD1_new_center, QD2_new_center = return_center(dbx, lorentzian=lorentzian, plot=plot)
        return dbx, QD1_new_center, QD2_new_center
    else:
        return dbx

def return_center(dbx, lorentzian=False, plot=True):
    RMGF = dbx.QD2_plunger_fine_Vdc.values
    LMGF = dbx.QD1_plunger_fine_Vdc.values
    center_x, center_y = find_center_hyperbolae(dbx.lockinRR_G.values, lorentzian=lorentzian)
    RMGFc_RR = (RMGF[-1] - RMGF[0]) * center_x / (len(RMGF) - 1) + RMGF[0] 
    LMGFc_RR = (LMGF[-1] - LMGF[0]) * center_y / (len(LMGF) - 1) + LMGF[0]
    if plot:
        plt.figure()
        dbx.lockinRR_G.plot()
        plt.scatter(LMGFc_RR, RMGFc_RR)
        plt.show()

    center_x, center_y = find_center_hyperbolae(dbx.lockinLL_G.values, lorentzian=lorentzian)
    RMGFc_LL = (RMGF[-1] - RMGF[0]) * center_x / (len(RMGF) - 1) + RMGF[0] 
    LMGFc_LL = (LMGF[-1] - LMGF[0]) * center_y / (len(LMGF) - 1) + LMGF[0]
    if plot:
        plt.figure()
        dbx.lockinLL_G.plot()
        plt.scatter(LMGFc_LL, RMGFc_LL)
        plt.show()

    QD1_new_center = (LMGFc_RR + LMGFc_LL)/2
    QD2_new_center =  (RMGFc_RR + RMGFc_LL)/2
    return QD1_new_center, QD2_new_center


# -

dbx, QD1_center, QD2_center = measure_avoided_crossing(QD1_center, QD1_range, QD2_center, QD2_range,  delay = delay_time, N=resolution, centering=True, lorentzian=False)


