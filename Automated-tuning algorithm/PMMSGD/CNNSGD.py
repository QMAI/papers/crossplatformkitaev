# from .QC_code_difference import *
from .test_data_code import *
import random
import numpy as np
import matplotlib.pyplot as plt
from qcodes import Parameter
from warnings import warn
from .cGAN_CNN_2DEG_difference import cgan, scaler, scaler_1, cnn_model, cnn_model_56
from .test_data_code import all_dbx, all_V_ABSs, all_Delta_minus_t
from skimage.transform import resize 

"""
Center finding and conductance functions
"""
def get_gLL_gRR_dbx(dbx, GLL_target, GRR_target):
    gLL = getattr(dbx, GLL_target).values
    gRR = getattr(dbx, GRR_target).values
    return gLL, gRR

def get_dbx_target(dbx, target):
    target_values = getattr(dbx, target).values
    return target_values

def remove_background(g, medians=3): # The idea is cutting all the values below the median, with some safety margin
    gflat = g.flatten()
    gmin = min(gflat)
    m = np.median(gflat) - gmin
    g_clean = np.array(g)
    g_clean[g < gmin + medians*m] = 0
    return g_clean

def _find_symmetric_extremes(boolean_array, center):
    b = np.array(boolean_array)
    n = len(b)
    for i in range(n):
        ind = int(np.round(2*center)) - i
        if ind > 0 and ind < n:
            b[i] = b[i] * b[ind]
        else:
            b[i] = False
        arange = np.arange(n)
        if int(b.sum())==0:
            return b, int(round(center)), int(round(center))
        return b, arange[b][0], arange[b][-1]

def find_center_hyperbolae(g, medians=3, iterations=3):
    gg = remove_background(g)
    Rmaxs = gg.argmax(axis=0)
    Rmins = gg.argmin(axis=0)
    Lmaxs = gg.argmax(axis=1)
    Lmins = gg.argmin(axis=1)
    right_maxs = Rmaxs[Rmaxs!=Rmins]
    if len(right_maxs)==0: # sanity check
        warn('Warning: no signal above background.')
        return gg.shape[0]//2, gg.shape[1]//2

    left_center = (right_maxs[0] + right_maxs[-1]) / 2
    left_maxs = Lmaxs[Lmaxs!=Lmins]
    right_center = (left_maxs[0] + left_maxs[-1]) / 2 # iterative improvement of center precision
    for i in range(iterations):
        right_filter, R0, R1 = _find_symmetric_extremes(Rmaxs!=Rmins, right_center)
        left_center = (Rmaxs[R0] + Rmaxs[R1]) / 2
        left_filter, L0, L1 = _find_symmetric_extremes(Lmaxs!=Lmins, left_center)
        right_center = (Lmaxs[L0] + Lmaxs[L1]) / 2
    return round(left_center), round(right_center)


"""
CNN functions
"""
def get_prediction_CNN(cnn_model, cgan, dbx, target, center_x = None, center_y = None):
    """
    If you don't specify centers, it uses Alberto's centering algo

    Parameters:
    - cnn_model: trained CNN to predict t-Delta using gLL and gRR as dual input
    - cgan: CGAN model, only used for preprocessing (cutting out the image frame)

    Returns:
    - t-Delta prediction of the CNN
    """
    if type(target) == list:
        target_arrays = [get_dbx_target(dbx, t) for t in target] #these are the gLL, gRR, etc.
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_arrays[0]) #could still change centering

        centered_target_arrays = [cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y) for target_array in target_arrays]
        
        normalised_target_arrays = [(target_array / np.amax(abs(target_array)) ) * 0.64 for target_array in centered_target_arrays]
        if len(target)==2:
            pred = cnn_model.predict(normalised_target_arrays)
        else:
            pred = cnn_model.predict(2*normalised_target_arrays)
    else:
        target_arrays = [get_dbx_target(dbx, target)] #these are the gLL, gRR, etc.
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_arrays[0]) #could still change centering

        centered_target_arrays = [cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y) for target_array in target_arrays]
        normalised_target_arrays = [(target_array / np.amax(abs(target_array)) ) * 0.64 for target_array in centered_target_arrays]
        print(normalised_target_arrays[0].shape)
        pred = cnn_model.predict(2*[normalised_target_arrays])


    return pred

def _make_list_or_not(A):
    if type(A) == list:
        if len(A) == 2:
            return A
        elif len(A) == 1:
            return 2*A
    else:
        return [A]


def retrain_CNN(cnn_model, cgan, dbx, target, labels, center_x = None, center_y = None, epochs=5, batch_size=32, validation_split=0.1):

    """
    (Re-) training of the CNN with new input data and labels. Dual input CNN with gLL and gRR simultaneously.

    Parameters:
    - cnn_model: trained CGAN model
    - data_gLL: normalized [0,1] differential conductance data (28x28 pixels), array dimension: [#samples, 28, 28, 1]
    - data_gRR: normalized [0,1] differential conductance data (28x28 pixels), array dimension: [#samples, 28, 28, 1]
    - labels: normalized [0,1] t-Delta ratio in form of a 1D array, array dimension: [#samples]
    - epochs: number of training iterations through full data set
    - batch_size: number of samples given to the cGAN at the same time
    - validation_split: splitting the training data each epoch randomly into a training + validation set
    """
    if type(target) != list:
        target = [target]
    if type(dbx) == xarray.Dataset: #only one entry
        total_data_array = []
        for t in target:
            target_array = get_dbx_target(dbx, t)
            if center_x == None or center_y == None:
                center_x, center_y = find_center_hyperbolae(target_array) #could still change centering
            centered_target_array = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y)
            normalised_target_array = (target_array / np.amax(abs(target_array)) ) * 0.64
            total_data_array.append(normalised_target_array)
        if len(target)==2:
            cnn_model.fit([total_data_array[0,:,0,:,:], total_data_array[1,:,0,:,:]], labels.reshape(-1,1), epochs=epochs, batch_size=batch_size,validation_split=validation_split)
        else:
            cnn_model.fit([total_data_array[0,:,0,:,:], total_data_array[0,:,0,:,:]] , labels.reshape(-1,1), epochs=epochs, batch_size=batch_size,validation_split=validation_split)
    else:
        labels=np.array(labels)
        dbx_list = dbx.copy()
        total_data_array = []
        for t in target:
            t_array = []
            for dbx in dbx_list:
                target_array = get_dbx_target(dbx, t)
                if center_x == None or center_y == None:
                    center_x, center_y = find_center_hyperbolae(target_array) #could still change centering
                centered_target_array = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y)
                normalised_target_array = (centered_target_array / np.amax(abs(centered_target_array)) ) * 0.64
                t_array.append(normalised_target_array)
            total_data_array.append(t_array)
        total_data_array=np.array(total_data_array)
        if len(target)==2:
            cnn_model.fit([total_data_array[0,:,0,:,:], total_data_array[1,:,0,:,:]], labels.reshape(-1,1), epochs=epochs, batch_size=batch_size,validation_split=validation_split)
        else:
            cnn_model.fit([total_data_array[0,:,0,:,:], total_data_array[0,:,0,:,:]] , labels.reshape(-1,1), epochs=epochs, batch_size=batch_size,validation_split=validation_split)

        return None


def get_prediction_CNN_56(cnn_model_56, dbx, target):
    """
    Parameters:
    - cnn_model: trained CNN to predict t-Delta using gLL and gRR as dual input

    Returns:
    - ratio prediction of the CNN
    """
    if type(target) == list:
        target_arrays = [get_dbx_target(dbx, t) for t in target] #these are the gLL, gRR, etc.
        normalised_target_arrays = [(target_array / np.amax(abs(target_array)) ) * 0.64 for target_array in target_arrays]        
        # resizing
        G_56 = np.zeros((len(target),56,56,1))
        for i in range(len(target)):
            G_56[i,:,:,0] = resize(normalised_target_arrays[i],(56,56))        
        if len(target)==2:            
            GLL_RR = np.multiply(G_56[0:1,:,:,:], G_56[1:2,:,:,:])
            GLL_RR = ( GLL_RR / np.amax(abs(GLL_RR)) ) * 0.64
            pred = cnn_model.predict([GLL_RR, GLL_RR])
        else:
            pred = cnn_model.predict([G_56[0,:,:,:], G_56[0,:,:,:]])
    else:
        # does not work yet
        target_arrays = [get_dbx_target(dbx, target)] #these are the gLL, gRR, etc.
        normalised_target_arrays = [(target_array / np.amax(abs(target_array)) ) * 0.64 for target_array in target_arrays]
        print(normalised_target_arrays[0].shape)
        pred = cnn_model.predict(2*[normalised_target_arrays])

    return pred


"""
cGAN functions
"""

def get_prediction_CGAN(cgan, dbx, target, x_points=25, center_x = None, center_y = None):
    """
    If you don't specify centers, it uses Alberto's centering algo

    Parameters:
    - cgan: trained CGAN model
    - x_points: number of different evaluations of the discriminator to obtain the rating function

    Returns:
    - t-Delta prediction of the CGAN averaged over gLL and gRR individually

    """
    if type(target) != list:
        target = [target]

    tD_cGAN_LL = 0
    for t in target:
        target_array = get_dbx_target(dbx, t)

        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_array)
        cut_image = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y, plot_images=False)
        cut_image_scale = cgan.scale_exp_data(cut_image)
        tD_cGAN = cgan.return_Delta_t(spectra=cut_image_scale, label=np.zeros((4,1)), scaler_1=scaler_1, n_plot=0, plot_orig = False, x_points=x_points)
        tD_cGAN += (tD_cGAN_LL - 0.35 ) * 4.5
    pred = tD_cGAN/len(target)
    return pred


def cut_and_scale_data(dbx, target, center_x = None, center_y = None):
    target_array = get_dbx_target(dbx, target)
    if center_x == None or center_y == None:
        center_x, center_y = find_center_hyperbolae(target_array)
    cut_image = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y, plot_images=False)
    cut_image_scale = cgan.scale_exp_data(cut_image)
    return cut_image_scale

def retrain_CGAN(cgan, dbx, target, labels, x_points=25, center_x = None, center_y = None, epochs=5, batch_size=4):
    """
    Retrains the given Conditional Generative Adversarial Network (cGAN) with new differential conductance data and t-Delta ratio labels. Example usage:
    PMMSGD.retrain_CGAN(PMMSGD.cgan,
                    PMMSGD.all_dbx[5],
                    ['lockinLL_G'],
                    PMMSGD.all_Delta_minus_t[5],
                    x_points=25, center_x = None, center_y = None, epochs=5, batch_size=4)

    Parameters:
    - cgan: Previously trained cGAN model.
    - dbx (list or xarray.dataarray): Data structure or database containing differential conductance information.
    - target: List or single value specifying the target conditions for training. If not a list, it will be converted into one.
    - labels: 1D array of normalized t-Delta ratios in the range [0,1]. Expected shape: [#samples, 1].
    - x_points (int, optional): Number of x points to consider. Defaults to 25.
    - center_x (float or None, optional): x-coordinate of the center for data processing. If None, it will be computed. Defaults to None.
    - center_y (float or None, optional): y-coordinate of the center for data processing. If None, it will be computed. Defaults to None.
    - epochs (int, optional): Number of training epochs. One epoch is a complete iteration over the full dataset. Defaults to 5.
    - batch_size (int, optional): Number of samples provided to the cGAN for each batch during training. Defaults to 4.

    Returns:
    - None: The function updates the cGAN model in-place and doesn't return any value.
    """
    if type(dbx) == xarray.Dataset: #only one entry
        if type(target) != list:
            target = [target]
        total_data_array = []
        for t in target:
            cut_image_scale = cut_and_scale_data(dbx, t, center_x = None, center_y = None)
            total_data_array.append(cut_image_scale)
        total_data_array = np.array(total_data_array)
        for L, t in zip(total_data_array, target): #this loops over all the targets
            print(f"training on {t}")
            cgan.train(L, labels, epochs=epochs, batch_size=batch_size)

    else:
        dbx_list = dbx.copy()
        if type(target) != list:
            target = [target]
        total_data_array = []
        for t in target:
            target_array = []
            for dbx in dbx_list:
                cut_image_scale = cut_and_scale_data(dbx, t, center_x = None, center_y = None)
                target_array.append(cut_image_scale)
            total_data_array.append(np.array(target_array))
        total_data_array= np.array(total_data_array)
        print(total_data_array.shape)
        for L, t in zip(total_data_array, target): #this loops over all the targets
            print(f"training on {t}")
            cgan.train(L, labels, epochs=epochs, batch_size=batch_size)

        return None

    # elif type(dbx) == list:
    #     dbx_list = dbx.copy()
    #     for dbx in dbx_list:

"""
SGD code
"""

def handle_first_epoch(V_ABS, min_step_size, first_step):
    """
    Perform the random step for the first epoch.

    Parameters:
    - w (float): Current parameter value.
    - min_step_size (float): Minimum step size for updates.

    Returns:
    - float: Updated parameter value.
    """
    if first_step == None:
        return  V_ABS + np.sign(np.random.randn()) * min_step_size
    else:
        return  V_ABS + first_step

def objective_function(dbx, classifier, target):
    """"
    classifier in ['CNN_differences', 'cGAN_differences', 'cGAN_ratio', 'CNN_ratio'], "Choose a correct classifier"
    """

    assert classifier in ['CNN_differences', 'cGAN_differences', 'cGAN_ratio', 'CNN_ratio'], "Choose a correct classifier"
    # CNN
    if classifier == 'CNN_differences':
        pred = get_prediction_CNN(cnn_model, cgan, dbx, target)
        obj = abs(pred[0,0]) #otherwise it will just keep going negative
        # print(pred)
    elif classifier == 'cGAN_differences':
        pred = get_prediction_CGAN(cgan, dbx, target)
        obj = abs(pred) #otherwise it will just keep going negative

    elif classifier == 'cGAN_ratio':
        print("new cGAN")
        # pred = get_prediction_CGAN(cgan, dbx, target)
        obj = 1-abs(pred) #otherwise it will just keep going negative

    elif classifier == 'CNN_ratio':
        print("new CNN")
        # pred = get_prediction_CGAN(cgan, dbx, target)
        obj = 1-abs(pred) #otherwise it will just keep going negative

    elif classifier == 'CNN_ratio_56':
        print("new CNN-56")
        pred = get_prediction_CNN_56(cnn_model_56, dbx, target)
        obj = 1-abs(pred) #otherwise it will just keep going negative
        
    return obj



def get_Delta_minus_t(dbx, classifier, target):
    """"
    classifier in ['CNN_differences', 'cGAN_differences', 'cGAN_ratio', 'CNN_ratio'], "Choose a correct classifier"
    """

    assert classifier in ['CNN_differences', 'cGAN_differences', 'cGAN_ratio', 'CNN_ratio'], "Choose a correct classifier"
    # CNN
    if classifier == 'CNN_differences':
        pred = get_prediction_CNN(cnn_model, cgan, dbx, target)
        obj = pred[0,0] #otherwise it will just keep going negative
        # print(pred)
    elif classifier == 'cGAN_differences':
        pred = get_prediction_CGAN(cgan, dbx, target)
        obj = pred #otherwise it will just keep going negative

    elif classifier == 'cGAN_ratio':
        print("new cGAN")
        # pred = get_prediction_CGAN(cgan, dbx, target)
        obj = pred #otherwise it will just keep going negative

    elif classifier == 'CNN_ratio':
        print("new CNN")
        # pred = get_prediction_CGAN(cgan, dbx, target)
        obj = pred #otherwise it will just keep going negative

    elif classifier == 'CNN_ratio_56':
        print("new CNN-56")
        pred = get_prediction_CNN_56(cnn_model_56, dbx, target)
        obj = pred #otherwise it will just keep going negative
        
    return obj


def compute_gradients(dbxs, V_ABSs, classifier, target):
    """
    Calculate gradients for a sequence of recent data points.

    Parameters:
    - recent_data (list of float): List of recent parameter values.
    - noise_magnitude (float): Magnitude of Gaussian noise.

    Returns:
    - list of float: Gradients for each consecutive pair in recent_data.
    """
    objs = np.array([objective_function(dbx, classifier, target) for dbx in dbxs])
    diff_objs = np.diff(objs)
    diff_VABS_s = np.diff(V_ABSs)
    with np.errstate(divide='ignore'):
        result = np.where(diff_VABS_s != 0, diff_objs/diff_VABS_s, 0)
    return result

def update_V_ABS(V_ABS, gradients, learning_rate, min_step_size, momentum, v, V_ABS_min, V_ABS_max):
    """
    Update w using momentum and ensure reflective boundaries.

    Parameters:
    - w: current value
    - v: current velocity
    - gradients: computed gradients
    - learning_rate, momentum, min_step_size: SGD parameters
    - lower_bound, upper_bound: boundaries for w

    Returns:
    - Updated w and v
    """

    for grad in gradients:
        # print(grad)
        v = momentum * v + learning_rate * grad
        if abs(v) < min_step_size:
        # if True:
            # print(1)
            v = np.sign(v) * min_step_size
        proposed_V_ABS = V_ABS- v

        if proposed_V_ABS < V_ABS_min:
            proposed_V_ABS = V_ABS_min + (V_ABS_min - proposed_V_ABS)  # Reflect off the boundary
            v = -v  # Reflect the momentum
        elif proposed_V_ABS > V_ABS_max:
            proposed_V_ABS = V_ABS_max - (proposed_V_ABS - V_ABS_max)  # Reflect off the boundary
            v = -v  # Reflect the momentum
        # if np.isnan(proposed_V_ABS):
        #     print(locals())
        V_ABS = proposed_V_ABS
    # print(V_ABS)
    return V_ABS, v


def SGD(dbx_start, V_ABS_start, V_ABS_parameter, learning_rate, epochs, min_step_size, tolerance, N, target, measure_func, measure_func_kwargs, momentum=0.9, break_condition='minimum', V_ABS_min=None, V_ABS_max=None, first_step=None, verbose=True, classifier='CNN', plot=False):
    """
    Stochastic Gradient Descent optimization function with momentum and utilizing past N data points.

    Parameters:
    - dbx_start (xarray.Dataset): Initial value of dbx.
    - V_ABS_start (float): Initial value for V_ABS.
    - V_ABS_parameter (qcodes.Parameter): Function to set the new gate value.
    - learning_rate (float): Learning rate for the SGD update.
    - epochs (int): Total number of iterations/epochs to run the optimization.
    - min_step_size (float): Minimum allowable step size for update.
    - tolerance (float): Tolerance value to determine early stopping.
    - N (int): Number of past data points to consider in the computation.
    - target (list or string): the data variable to use for prediction. Can be a string or a list
    - measure_func (function): Function to perform the CSD measurement, needs to output a dbx.
    - momentum (float, optional): Momentum term for the optimization. Default is 0.9.
    - break_condition (str, optional): The condition to break the loop, either 'minimum' or 'slope'. Default is 'minimum'.
    - V_ABS_min (float, optional): Minimum allowable value for V_ABS.
    - V_ABS_max (float, optional): Maximum allowable value for V_ABS.
    - verbose (bool, optional): Flag to print out information during the optimization. Default is True.
    - classifier (str, optional): The type of classifier used in the objective function. Default is 'CNN'.
    - plot (bool): Flag to plot measured values for each epoch

    Returns:
    - list of float: V_ABS values over epochs.
    - list of float: Objective function values over epochs.
    - float: The current objective function value.
    - int: The epoch at which the optimization was terminated.
    """

    assert min_step_size>1e-6, f"min step size < {1e-6}, this is below DAC quantisation"
    try:
        V_ABS = V_ABS_start
        V_ABSs = [V_ABS_start]
        dbx_start = measure_func(**measure_func_kwargs) #perform CSD measurement
        if verbose: 
            run_id = dbx_start.attrs['run_id']
            print(f"start of run with id {run_id}")
        dbxs = [dbx_start]
        current_obj_val = objective_function(dbx_start, classifier, target)
        obj_fun_values = [current_obj_val]
        epoch=0
        v = 0  # Initialize velocity (momentum term)



        for epoch in range(epochs):
            if epoch == 0:
                V_ABS = handle_first_epoch(V_ABS, min_step_size, first_step)
            else:
                recent_V_ABSs = V_ABSs[-N:]
                recent_dbxs = dbxs[-N:]
                gradients = compute_gradients(recent_dbxs, recent_V_ABSs, classifier, target)
                np.random.shuffle(gradients)
                # V_ABS, v = update_V_ABS(V_ABS, gradients, learning_rate, min_step_size, momentum, v)
                V_ABS, v = update_V_ABS(V_ABS, gradients, learning_rate, min_step_size, momentum, v, V_ABS_min, V_ABS_max)
                if verbose: print(f"new V_ABS= {V_ABS:.5f}")

            V_ABS_parameter(V_ABS) #set new gate value
            dbx = measure_func(**measure_func_kwargs) #perform CSD measurement
            print(plot)
            if plot:
                if type(target) == str:
                    fig, ax = plt.subplots(1, len(target), figsize=(len(target)*4, 3))

                    getattr(dbx, target).plot(ax=ax, add_colorbar=False)
                    ax.set_title(f'{V_ABS_parameter.name} ='+ f'{1e3*V_ABS_parameter():.2f} mV')
                elif len(target) == 1:
                    fig, ax = plt.subplots(1, len(target), figsize=(len(target)*4, 3))

                    getattr(dbx, target[0]).plot(ax=ax, add_colorbar=False)
                    ax.set_title(f'{V_ABS_parameter.name} =' +f'{1e3*V_ABS_parameter():.2f} mV')
                else:
                    fig, ax = plt.subplots(1, len(target), figsize=(len(target)*4, 3))
                    ax[0].set_title(f'{V_ABS_parameter.name} =' +f'{1e3*V_ABS_parameter():.2f} mV')
                    for i, t in enumerate(target):
                        getattr(dbx, t).plot(ax=ax[i], add_colorbar=False)
            current_obj_val = objective_function(dbx, classifier, target)
            if verbose: print(f"prediction: t-Delta= {get_Delta_minus_t(dbx, classifier, target):.2f}")
            obj_fun_values.append(current_obj_val)
            V_ABSs.append(V_ABS)
            dbxs.append(dbx)


            if break_condition=='minimum':
                if epoch > 0 and abs(current_obj_val) < tolerance:
                    if verbose: print("found minimum at V_ABS = {0} after {1} epochs for run_id={2}".format(V_ABS, epoch, dbx.attrs['run_id']))
                    break
            elif break_condition=='slope':
                if abs(current_obj_val - obj_fun_values[-2]) < tolerance:
                    if verbose: print("found minimum at V_ABS = {0} after {1} epochs for run_id={2}".format(V_ABS, epoch, dbx.attrs['run_id']))
                    break
        return V_ABSs, np.array(obj_fun_values).flatten(), current_obj_val, epoch
    except KeyboardInterrupt:
        print('Interrupted')
        return V_ABSs, np.array(obj_fun_values).flatten(), current_obj_val, epoch

"""
Plotting and logging code
"""

import json
import numpy as np
import matplotlib.pyplot as plt
from xarray import Dataset
import qcodes as qc
from matplotlib.backends.backend_pdf import PdfPages

def get_settings_by_db(dbx, verbose=True):
    ss = json.loads(dbx.attrs['snapshot'])
    dev_snapshot = ss['station']['instruments']['dev']
    gate_dc = {}
    if verbose: print("Gates")
    for gate_bias_string in ['LOG', 'LMG', 'LIG', 'PG', 'RIG', 'RMG', 'ROG', 'LMG_fine', 'RMG_fine']:
        try:
            gate_bias_ss = dev_snapshot['submodules'][gate_bias_string]
            gate_bias_ss_AC = gate_bias_ss['parameters']['Vac']['value']
            gate_bias_ss_DC = gate_bias_ss['parameters']['Vdc']['value']
            if verbose: print(gate_bias_string + " DC : {} V".format(gate_bias_ss_DC))
            gate_dc[gate_bias_string] = round(gate_bias_ss_DC, 5)
            if gate_bias_ss_AC:
                if verbose: print(gate_bias_string + " AC: {} V".format(gate_bias_ss_AC))
        except KeyError:
            pass
    if verbose: print()
    if verbose: print("Biases")
    for gate_bias_string in [ 'biasL', 'biasR']:
        try:
            gate_bias_ss = dev_snapshot['submodules'][gate_bias_string]
            gate_bias_ss_AC = gate_bias_ss['parameters']['Vac']['value']
            gate_bias_ss_DC = gate_bias_ss['parameters']['Vdc']['value']
            if verbose: print(gate_bias_string + " DC : {} V".format(gate_bias_ss_DC))
            if gate_bias_ss_AC:
                if verbose: print(gate_bias_string + " AC: {} V".format(gate_bias_ss_AC))
        except KeyError:
            pass
    if verbose: print()
    # for lockin_string in ['lockinLL', 'lockinLR', 'lockinRL', 'lockinRR']:
        
    for resonator_string in ['resonatorL', 'resonatorR']:
        try:
            resonator_ss = dev_snapshot['submodules'][resonator_string]
            if verbose: print(resonator_string)
            for param in ['frequency', 'Vout_pk', 'Vout_rms', 'power']:
                val_to_set = resonator_ss['parameters'][param]['value']
                if verbose: print(param+": "+str(val_to_set))
            if verbose: print()
        except KeyError:
            pass

    return gate_dc

def convert_to_xarray(run_id):
    # Get the dataset for the run_id
    db = qc.load_by_id(run_id)
    xr_ds = db.to_xarray_dataset()
    gate_dc = get_settings_by_db(xr_ds, False)
    PG_val = gate_dc['PG']
    Dmt = get_Delta_minus_t(xr_ds, 'CNN_differences', ['lockinLL_G', 'lockinRR_G'])
    return xr_ds, PG_val, Dmt

def plot_and_save(run_ids):
    path, db_name = os.path.split(qc.config.current_config.core.db_location)
    filename = f"{db_name}_runs_{run_ids[0]}_to_{run_ids[-1]}.pdf"
    with PdfPages(filename) as pdf:
        for i in range(0, len(run_ids), 3):  # Process 3 run_ids at a time
            fig, axs = plt.subplots(3, 2, figsize=(12, 9))

            for j in range(3):
                if i + j >= len(run_ids):
                    break

                run_id = run_ids[i + j]
                xr_ds, PG_val, Dmt = convert_to_xarray(run_id)

                # Plot lockinLL_G
                if 'lockinLL_G' in xr_ds:
                    xr_ds['lockinLL_G'].plot(ax=axs[j, 0])
                    axs[j, 0].set_title(f'Run ID: {run_id} Delta-t: {Dmt:.2f} - lockinLL_G')

                # Plot lockinRR_G
                if 'lockinRR_G' in xr_ds:
                    xr_ds['lockinRR_G'].plot(ax=axs[j, 1])
                    axs[j, 1].set_title(f'PG: {PG_val}V - lockinRR_G')

            plt.tight_layout()
            pdf.savefig(fig)
            plt.close(fig)

