import numpy as np
import cGAN_CNN_2DEG_difference as cGAN
import qcodes as qc
import matplotlib.pyplot as plt
from skimage.transform import resize
import tensorflow as tf
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense
from tensorflow.keras.models import Model


def load_cgan():
    cgan = cGAN.CGAN_CNN()
    label_all = np.load("Gammas_all.npy")
    
    label = np.copy(label_all)[:,0]
    for i in range (len(label_all)):
        label[i] = label_all[i,0] - label_all[i,1] # = Gamma_odd / Gamma_even

    label = label.reshape(-1,1)
    
    # set and fit scaler for cGAN output
    scaler, scaler_1 = cgan.set_scaler()    
    scaler_1 = cgan.fit_scaler(scaler_1, label[:,0:1])
    
    # load weights of pretrained model (here: for Delta/t ratio)
    # cgan = cgan.load_weights_difference(cgan)
    
    labels = np.array([1])
    labels = labels.reshape(1,1)
    
    cgan.exp_labels = label

    cgan = cgan.load_weights_difference(cgan)
    cgan.scaler_1 = scaler_1

    return cgan, scaler, scaler_1
    # global cgan, scaler_1, exp_labels

    
    
    
def predict_dbx(dbx, G_variable, cgan, scale_max=0.65, scale_grad=1.0, x_points=25, batch_size=128, plot=False):
    dbG = getattr(dbx, G_variable).values
    if plot:
        getattr(dbx, G_variable).plot()
        
    data = cgan.resize_image(dbG)
    cut_image_scale = cgan.scale_exp_data(data, scale_max, scale_grad)  
    prediction = cgan.predict_t_Delta(spectra=cut_image_scale, label=cgan.exp_labels, scaler_1=cgan.scaler_1, n_plot=0, x_points=x_points, batch_size=batch_size)
    return prediction


def param_estimation(self, spectra, label, scaler_1, n_plot=0, x_points=25, batch_size=128):
        """estimate Hamiltonian parameters (here: Delta/t ratio)
            --- still have to work on this part --- """        
        # initialize
        d_params = np.zeros(x_points)
        batch = np.random.randint(0, spectra.shape[0], size=batch_size)
        image_batch_test = spectra[batch]
        label_batch_test = label[batch]
        
        # feed new data
        n_plot = n_plot
        test_label_batch = np.zeros((batch_size,1))
        test_label_batch[0,0] = label[n_plot,0]  
        delta = scaler_1.inverse_transform(test_label_batch)
        
        d_test_image = spectra[n_plot,:,:,0:] 
        self.create_3D_plot(d_test_image[:,:,0])
        
        for i in range(x_points):
            d_test_label = np.array((i/x_points))
            image_batch_test[0,:] = d_test_image
            label_batch_test[0,:] = d_test_label
            d_pred = self.discriminator.predict([image_batch_test,label_batch_test])[0]
            d_params[i] = d_pred
        
        
        # Interval of Delta-t values of the training data
        x_min = 0.0
        x_max = 0.8
        
        x_grid = np.linspace(x_min, x_max, x_points)
        
        rescale_pred = np.argmax(d_params)/(x_points-1)
        rescale_pred = rescale_pred.reshape(-1,1)
        rescale_pred = scaler_1.inverse_transform(rescale_pred)
        
        fig, ax = plt.subplots(dpi=150 , figsize=[4,4])
        ax.plot(x_grid, d_params)
        # ax.set_ylim(min(d_params),max(d_params)+0.1)
        ax.set_xlim(x_min,x_max)
        plt.axvline(x=rescale_pred[0,0],color='purple', ls='--', lw=1.5,
                    label='max_pred = {:.2f}'.format(rescale_pred[0,0]))
        plt.axvline(x=scaler_1.inverse_transform(test_label_batch)[0,0],color='green', ls=':', lw=1.5, 
                    label='max_real= {:.2f}'.format(scaler_1.inverse_transform(test_label_batch)[0,0]))
        plt.legend()
        plt.xlabel("$\Gamma_{even} - \Gamma_{odd}$ ")
        plt.ylabel("Discriminator rating")
        plt.show()
           
        return None 
        
        
def CNN_difference():
	# Define input layers
	input_1 = Input(shape=(28, 28, 1))
	input_2 = Input(shape=(28, 28, 1))

	# Define convolutional layers for image 1
	conv1_1 = Conv2D(32, (3, 3), activation='relu', padding='same')(input_1)
	pool1_1 = MaxPooling2D((2, 2))(conv1_1)
	conv2_1 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1_1)
	pool2_1 = MaxPooling2D((2, 2))(conv2_1)
	flat1 = Flatten()(pool2_1)

	# Define convolutional layers for image 2
	conv1_2 = Conv2D(32, (3, 3), activation='relu', padding='same')(input_2)
	pool1_2 = MaxPooling2D((2, 2))(conv1_2)
	conv2_2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1_2)
	pool2_2 = MaxPooling2D((2, 2))(conv2_2)
	flat2 = Flatten()(pool2_2)

	# Concatenate the flattened outputs from the two convolutional paths
	merged = tf.keras.layers.concatenate([flat1, flat2])

	# Define fully connected layersS
	fc1 = Dense(128, activation='relu')(merged)
	output = Dense(1, activation='tanh')(fc1)

	# Define the model with two input layers and one output layer
	model = Model(inputs=[input_1, input_2], outputs=output)

	# Compile the model with categorical crossentropy loss and Adam optimizer
	model.compile(loss='MSE', optimizer='adam')

	model.load_weights('network_params/CNN_difference_local_2.h5')
	
	return model
