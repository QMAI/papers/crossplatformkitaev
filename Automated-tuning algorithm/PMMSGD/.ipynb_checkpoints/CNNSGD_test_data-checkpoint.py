# from .QC_code_difference import *
from .test_data_code import *
import random
import numpy as np
import matplotlib.pyplot as plt
import proplot as pplt
from qcodes import Parameter
from warnings import warn
from .cGAN_CNN_2DEG_difference import cgan, scaler, scaler_1, cnn_model
from .test_data_code import all_dbx, all_V_ABSs, all_Delta_minus_t

"""
Center finding and conductance functions
"""
def get_gLL_gRR_dbx(dbx, GLL_target, GRR_target):
    gLL = getattr(dbx, GLL_target).values
    gRR = getattr(dbx, GRR_target).values
    return gLL, gRR

def get_dbx_target(dbx, target):
    target_values = getattr(dbx, target).values
    return target_values

def remove_background(g, medians=3): # The idea is cutting all the values below the median, with some safety margin 
    gflat = g.flatten() 
    gmin = min(gflat) 
    m = np.median(gflat) - gmin 
    g_clean = np.array(g) 
    g_clean[g < gmin + medians*m] = 0 
    return g_clean

def _find_symmetric_extremes(boolean_array, center): 
    b = np.array(boolean_array) 
    n = len(b) 
    for i in range(n): 
        ind = int(np.round(2*center)) - i 
        if ind > 0 and ind < n: 
            b[i] = b[i] * b[ind] 
        else: 
            b[i] = False 
        arange = np.arange(n) 
        if int(b.sum())==0: 
            return b, int(round(center)), int(round(center)) 
        return b, arange[b][0], arange[b][-1]
    
def find_center_hyperbolae(g, medians=3, iterations=3):
    gg = remove_background(g)
    Rmaxs = gg.argmax(axis=0) 
    Rmins = gg.argmin(axis=0) 
    Lmaxs = gg.argmax(axis=1) 
    Lmins = gg.argmin(axis=1)
    right_maxs = Rmaxs[Rmaxs!=Rmins] 
    if len(right_maxs)==0: # sanity check 
        print('Warning: no signal above background.') 
        return gg.shape[0]//2, gg.shape[1]//2

    left_center = (right_maxs[0] + right_maxs[-1]) / 2 
    left_maxs = Lmaxs[Lmaxs!=Lmins] 
    right_center = (left_maxs[0] + left_maxs[-1]) / 2 # iterative improvement of center precision 
    for i in range(iterations): 
        right_filter, R0, R1 = _find_symmetric_extremes(Rmaxs!=Rmins, right_center) 
        left_center = (Rmaxs[R0] + Rmaxs[R1]) / 2 
        left_filter, L0, L1 = _find_symmetric_extremes(Lmaxs!=Lmins, left_center) 
        right_center = (Lmaxs[L0] + Lmaxs[L1]) / 2
    return round(left_center), round(right_center)


""" 
CNN functions
"""
def get_prediction_CNN(cnn_model, cgan, dbx, target, center_x = None, center_y = None):
    """
    If you don't specify centers, it uses Alberto's centering algo
    
    Parameters:
    - cnn_model: trained CNN to predict t-Delta using gLL and gRR as dual input
    - cgan: CGAN model, only used for preprocessing (cutting out the image frame)
    
    Returns:
    - t-Delta prediction of the CNN
    """
    if type(target) == list:
        target_arrays = [get_dbx_target(dbx, t) for t in target] #these are the gLL, gRR, etc.
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_arrays[0]) #could still change centering

        centered_target_arrays = [cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y) for target_array in target_arrays]
        normalised_target_arrays = [(target_array / np.amax(abs(target_array)) ) * 0.64 for target_array in centered_target_arrays]
        if len(target)==2:
            pred = cnn_model.predict(normalised_target_arrays) * 6  
        else:
            pred = cnn_model.predict(2*normalised_target_arrays) * 6  
    else:
        target_arrays = [get_dbx_target(dbx, target)] #these are the gLL, gRR, etc.
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_arrays[0]) #could still change centering

        centered_target_arrays = [cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y) for target_array in target_arrays]
        normalised_target_arrays = [(target_array / np.amax(abs(target_array)) ) * 0.64 for target_array in centered_target_arrays]
    
        pred = cnn_model.predict(2*[normalised_target_arrays]) * 6  
        
     
    return pred

def retrain_CNN(cnn_model, data_gLL, data_gRR, labels, epochs=5, batch_size=32, validation_split=0.1):
    """
    (Re-) training of the CNN with new input data and labels. Dual input CNN with gLL and gRR simultaneously.
    
    Parameters:
    - cnn_model: trained CGAN model
    - data_gLL: normalized [0,1] differential conductance data (28x28 pixels), array dimension: [#samples, 28, 28, 1]  
    - data_gRR: normalized [0,1] differential conductance data (28x28 pixels), array dimension: [#samples, 28, 28, 1]                                                                                                                                                                    
    - labels: normalized [0,1] t-Delta ratio in form of a 1D array, array dimension: [#samples]
    - epochs: number of training iterations through full data set
    - batch_size: number of samples given to the cGAN at the same time    
    - validation_split: splitting the training data each epoch randomly into a training + validation set
    """
    cnn_model.fit([data_gLL, data_gRR], labels, epochs=epochs, batch_size=batch_size,validation_split=validation_split)
    
    return None


"""
cGAN functions
"""

def get_prediction_CGAN(cgan, dbx, target, x_points=25, center_x = None, center_y = None):
    """
    If you don't specify centers, it uses Alberto's centering algo
    
    Parameters:
    - cgan: trained CGAN model
    - x_points: number of different evaluations of the discriminator to obtain the rating function
        
    Returns:
    - t-Delta prediction of the CGAN averaged over gLL and gRR individually
    
    """
    
    
    
    
    if type(target) != list:
        target = [target]
    
    tD_cGAN_LL = 0
    for t in target:
        target_array = get_dbx_target(dbx, t)
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_array)
        cut_image = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y, plot_images=False)
        cut_image_scale = cgan.scale_exp_data(cut_image) 
        tD_cGAN = cgan.return_Delta_t(spectra=cut_image_scale, label=np.zeros((4,1)), scaler_1=scaler_1, n_plot=0, plot_orig = False, x_points=x_points)
        tD_cGAN += (tD_cGAN_LL - 0.35 ) * 4.5
    pred = tD_cGAN/len(target)
    return pred

def retrain_CGAN(cgan, data, labels, epochs=5, batch_size=4):
    """
    (Re-) training of the cGAN with new input data and labels.
    
    Parameters:
    - cgan: trained CGAN model
    - data: normalized [0,1] differential conductance data (28x28 pixels), array dimension: [#samples, 28, 28, 1].
            cGAN is trained individually on GLL and (or) GRR                                                                                
    - labels: normalized [0,1] t-Delta ratio in form of a 1D array, array dimension: [#samples, 1]
    - epochs: number of training iterations through full data set
    - batch_size: number of samples given to the cGAN at the same time    
    """
    cgan.train(data, labels, epochs=epochs, batch_size=batch_size)
       
    return None
    



"""
SGD code
"""

def handle_first_epoch(V_ABS, min_step_size):
    """
    Perform the random step for the first epoch.
    
    Parameters:
    - w (float): Current parameter value.
    - min_step_size (float): Minimum step size for updates.
    
    Returns:
    - float: Updated parameter value.
    """
    return  V_ABS + np.sign(np.random.randn()) * min_step_size

def objective_function_test_data(V_ABS, classifier, target):
    i = closest_index(all_V_ABSs, V_ABS)
    dbx = all_dbx[i]
    # CNN
    if classifier == 'CNN':
        pred = get_prediction_CNN(cnn_model, cgan, dbx, target)
        obj = abs(pred[0,0]) #otherwise it will just keep going negative
        # print(pred)
    elif classifier == 'cGAN':
        pred = get_prediction_CGAN(cgan, dbx, target)
        obj = abs(pred) #otherwise it will just keep going negative
    return obj

def get_Delta_minus_t_test_data(V_ABS, classifier, target):
    i = closest_index(all_V_ABSs, V_ABS)
    dbx = all_dbx[i]
    # CNN
    if classifier == 'CNN':
        pred = get_prediction_CNN(cnn_model, cgan, dbx, target)
        obj = pred[0,0]
        # print(pred)
    elif classifier == 'cGAN':
        pred = get_prediction_CGAN(cgan, dbx, target)
        obj = pred
    return obj

def compute_gradients(recent_data, classifier, target):
    """
    Calculate gradients for a sequence of recent data points.
    
    Parameters:
    - recent_data (list of float): List of recent parameter values.
    - noise_magnitude (float): Magnitude of Gaussian noise.
    
    Returns:
    - list of float: Gradients for each consecutive pair in recent_data.
    """
    objs = np.array([objective_function_test_data(V_ABS, classifier, target) for V_ABS in recent_data])
    diff_objs = np.diff(objs)
    diff_VABS_s = np.diff(recent_data)
    result = np.where(diff_VABS_s != 0, diff_objs/diff_VABS_s, 0)
    return result

def update_V_ABS(V_ABS, V_ABS_parameter, gradients, learning_rate, min_step_size, momentum, v, V_ABS_min, V_ABS_max):
    """
    Update w using momentum and ensure reflective boundaries.

    Parameters:
    - w: current value
    - v: current velocity
    - gradients: computed gradients
    - learning_rate, momentum, min_step_size: SGD parameters
    - lower_bound, upper_bound: boundaries for w

    Returns:
    - Updated w and v
    """
    
    for grad in gradients:
        # print(grad)
        v = momentum * v + learning_rate * grad
        if abs(v) < min_step_size:
        # if True:
            # print(1)
            v = np.sign(v) * min_step_size
        proposed_V_ABS = V_ABS- v
        
        if proposed_V_ABS < V_ABS_min:
            proposed_V_ABS = V_ABS_min + (V_ABS_min - proposed_V_ABS)  # Reflect off the boundary
            v = -v  # Reflect the momentum
        elif proposed_V_ABS > V_ABS_max:
            proposed_V_ABS = V_ABS_max - (proposed_V_ABS - V_ABS_max)  # Reflect off the boundary
            v = -v  # Reflect the momentum
        # if np.isnan(proposed_V_ABS):
        #     print(locals())
        V_ABS = proposed_V_ABS
    # print(V_ABS)
    return V_ABS, v
    
    
def SGD_test_data(i_start, V_ABS_parameter, learning_rate, epochs, min_step_size, tolerance, N, momentum=0.9, break_condition='minimum', V_ABS_min=None, V_ABS_max=None, verbose=True, classifier='CNN', target=['lockinLL_G, lockinRR_G']):
    """
    Stochastic Gradient Descent with momentum and using past N data points.
    
    Parameters:
    - w_start (float): Initial parameter value.
    - learning_rate, epochs, min_step_size, tolerance, N, noise_magnitude, momentum: Hyperparameters.
    
    Returns:
    - list of float: Parameter values over epochs.
    - list of float: Objective function values over epochs.
    """
    
    dbx_start = all_dbx[i_start]
    V_ABS_start = all_V_ABSs[i_start]
    V_ABS = V_ABS_start
    V_ABS_parameter(V_ABS) #remove this before launch
    
    V_ABSs = [V_ABS_start]
    obj_fun_values = [objective_function_test_data(V_ABS, classifier, target)]
    data = [V_ABS_start]
    v = 0  # Initialize velocity (momentum term)

    for epoch in range(epochs):
        if epoch == 0:
            V_ABS = handle_first_epoch(V_ABS, min_step_size)
        else:
            recent_data = data[-N:]
            gradients = compute_gradients(recent_data, classifier, target)
            np.random.shuffle(gradients)
            # V_ABS, v = update_V_ABS(V_ABS, gradients, learning_rate, min_step_size, momentum, v)
            V_ABS, v = update_V_ABS(V_ABS, V_ABS_parameter, gradients, learning_rate, min_step_size, momentum, v, V_ABS_min, V_ABS_max)
            V_ABS_parameter(V_ABS)
        try:
            current_obj_val = objective_function_test_data(V_ABS, classifier, target)
        except IndexError as e:
            print(e)
            print("V_ABSs", V_ABSs)
            print("recent data", recent_data)
            print("gradients", gradients)
        obj_fun_values.append(current_obj_val)
        V_ABSs.append(V_ABS)
        data.append(V_ABS)
        if break_condition=='minimum':
            if epoch > 0 and abs(current_obj_val) < tolerance:
                if verbose: print("found minimum at V_ABS = {0} after {1} epochs".format(V_ABS, epoch))
                break
        elif break_condition=='slope':
            if abs(current_obj_val - obj_fun_values[-2]) < tolerance:
                if verbose: print("found minimum at V_ABS = {0} after {1} epochs".format(V_ABS, epoch))
                break

    return V_ABSs, np.array(obj_fun_values).flatten(), current_obj_val, epoch

def globalize_data(_all_dbx, _all_V_ABSs, _all_Delta_minus_t):
    global all_dbx, all_V_ABSs, all_Delta_minus_t
    all_dbx = _all_dbx
    all_V_ABSs = _all_V_ABSs
    all_Delta_minus_t = _all_Delta_minus_t
 
