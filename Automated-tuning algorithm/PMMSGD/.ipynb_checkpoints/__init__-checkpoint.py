from .test_data_code import all_dbx, all_V_ABSs, all_Delta_minus_t, all_Delta_over_t, all_Delta_plus_t
# from .CNNSGD import globalize_data, objective_function

# from .cGAN_CNN_2DEG_difference import load_cgan, CNN_difference, cgan, scaler, scaler_1, model
from .cGAN_CNN_2DEG_difference import cgan, scaler, scaler_1, cnn_model
from .CNNSGD_test_data import objective_function_test_data, get_Delta_minus_t_test_data, SGD_test_data

from .CNNSGD import objective_function, get_Delta_minus_t, SGD, retrain_CGAN, retrain_CNN
print("PMMSGD module correctly initialized")
# from test_data_code import load_test_data
# from .QC_code_difference import load_cgan, CNN_difference