# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.15.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
# %load_ext autoreload
# %autoreload 2
import random
import os
import xarray
import numpy as np
import matplotlib.pyplot as plt
# import proplot as pplt
import PMMSGD
from qcodes import Parameter, validators


class V_ABS_parameter(Parameter):
    def __init__(self, name, V_ABS_min, V_ABS_max):
        if V_ABS_min != None:
            vals = validators.Numbers(min_value=V_ABS_min, max_value=V_ABS_max)
            super().__init__(name=name, vals=vals)
        else:
            super().__init__(name=name)
        self.V_ABS = 0
    def get_raw(self):
        return self.V_ABS

    def set_raw(self, V_ABS):
        self.V_ABS = V_ABS


# +
# def filter_duplicates(all_dbx, all_V_ABSs, all_Delta_minus_t, all_Delta_over_t, all_Delta_plus_t):
#     """
#     Remove duplicates based on all_V_ABSs and return filtered lists.

#     Parameters:
#         all_dbx (list): List of datasets.
#         all_V_ABSs (list): List of V_ABS attributes.
#         all_Delta_minus_t (list): List of Delta_minus_t attributes.

#     Returns:
#         tuple: Filtered lists of datasets, V_ABSs, and Delta_minus_ts.
#     """
#     dup_idx = remove_duplicates_get_indices(all_V_ABSs)

#     all_dbx = [x for i, x in enumerate(all_dbx) if i not in dup_idx]
#     all_Delta_minus_t = [x for i, x in enumerate(all_Delta_minus_t) if i not in dup_idx]
#     all_V_ABSs = [x for i, x in enumerate(all_V_ABSs) if i not in dup_idx]
#     all_Delta_plus_t = [x for i, x in enumerate(all_Delta_plus_t) if i not in dup_idx]
#     all_Delta_over_t = [x for i, x in enumerate(all_Delta_over_t) if i not in dup_idx]

#     return all_dbx, all_V_ABSs, all_Delta_minus_t, all_Delta_over_t, all_Delta_plus_t,
# all_dbx_dups, all_V_ABSs_dups, all_Delta_minus_t_dups, all_Delta_over_t_dups, all_Delta_plus_t_dups
# -

all_dbx = []
all_V_ABSs = []
all_Delta_minus_t = []
all_Delta_over_t = []
all_Delta_plus_t = []
directory = "../2DEG_labelled_data"
for file in os.listdir(directory):
    dbx = xarray.load_dataset(os.path.join(directory, file), engine='h5netcdf')
    all_dbx.append(dbx)

    param_tD = - getattr(dbx, 'Delta_minus_t') * 10000 * 2
    all_Delta_minus_t.append(param_tD)

    V_ABS = getattr(dbx, 'V_ABS')
    all_V_ABSs.append(V_ABS)

    Delta_plus_t = getattr(dbx, 'Delta_plus_t')* 10000 * 2
    all_Delta_plus_t.append(Delta_plus_t)

    Delta_over_t = getattr(dbx, 'Delta/t')
    all_Delta_over_t.append(Delta_over_t)

# # Test data

PMMSGD.get_Delta_minus_t()

# +
fig, ax = plt.subplots(1, 2, figsize=(6,2))
s=3
ax[0].scatter(PMMSGD.all_V_ABSs, PMMSGD.all_Delta_minus_t, label=r'$\Delta-t$', s=s)
ax[0].scatter(PMMSGD.all_V_ABSs, PMMSGD.all_Delta_plus_t, label=r'$\Delta+t$', s=s)
ax[0].axhline(0, c='k')
ax[0].set_xlabel(r'$V_\mathrm{ABS}$')
ax[0].legend()

ax[1].scatter(PMMSGD.all_V_ABSs, PMMSGD.all_Delta_over_t, s=s)
ax[1].axhline(1, c='k')
ax[1].set_xlabel(r'$V_\mathrm{ABS}$')
ax[1].set_ylabel(r'$\Delta/t$')
plt.show()

# +
fig, ax = plt.subplots(1, 2, figsize=(6,2))
s=3
ax[0].scatter(PMMSGD.all_V_ABSs, PMMSGD.all_Delta_minus_t, label=r'$\Delta-t$', s=s)
ax[0].scatter(PMMSGD.all_V_ABSs, PMMSGD.all_Delta_plus_t, label=r'$\Delta+t$', s=s)
ax[0].axhline(0, c='k')
ax[0].set_xlabel(r'$V_\mathrm{ABS}$')
ax[0].legend()

ax[1].scatter(PMMSGD.all_V_ABSs, np.array(PMMSGD.all_Delta_minus_t)/np.array(PMMSGD.all_Delta_plus_t), s=s, label=r'$(\Delta-t)/(\Delta+t)$')
ax[1].set_xlabel(r'$V_\mathrm{ABS}$')
ax[1].legend()
# ax[1].set_ylabel(r'$\Delta/t$')
plt.show()

# +
plt.figure()
s=3
plt.scatter(PMMSGD.all_V_ABSs, np.array(PMMSGD.all_Delta_minus_t)/np.array(PMMSGD.all_Delta_plus_t), label=r'$\Delta-t$', s=s)
plt.scatter(PMMSGD.all_V_ABSs, np.array(PMMSGD.all_Delta_minus_t), label=r'$\Delta-t$', s=s)


plt.show()
# -

cGAN_predictions = []
CNN_predictions = []
for V_ABS in PMMSGD.all_V_ABSs:
    cGAN_predictions.append(PMMSGD.get_Delta_minus_t_test_data(V_ABS, classifier='cGAN', target='lockinLL_G'))
    CNN_predictions.append(PMMSGD.get_Delta_minus_t_test_data(V_ABS, classifier='CNN', target='lockinLL_G'))


plt.figure(figsize=(8,3))
plt.scatter(PMMSGD.all_V_ABSs, PMMSGD.all_Delta_minus_t, c='b', s=5, label=r'$|t-\Delta|$ measured')
# plt.scatter(PMMSGD.all_V_ABSs, cGAN_predictions, c='r', s=5, label=r'$|t-\Delta|$ cGAN')
plt.scatter(PMMSGD.all_V_ABSs, CNN_predictions, c='k', s=5, label=r'$|t-\Delta|$ CNN')
plt.axhline(0)
plt.legend()


# # Test run

PMMSGD.all_dbx[0]

# +

params = dict(
    i_start = random.choice(range(len(PMMSGD.all_dbx))),
    learning_rate = 1e-7, #1e-7 is good
    epochs = 30,
    min_step_size = 60e-6, #60 uV
    tolerance =.045,
    N = 5,  # Use the past 5 data points, for example,
    momentum = .5e-1,
    break_condition='minimum',
    V_ABS_min=-0.62431,
    V_ABS_max=-.62,
    verbose=True,
    classifier = 'CNN',
    target=['lockinRR_G', 'lockinLL_G']

)
# V_ABS_dc = V_ABS_parameter("V_ABS", params['V_ABS_min'], params['V_ABS_max'])
V_ABS_dc = V_ABS_parameter("V_ABS", None, None)
params['V_ABS_parameter'] = V_ABS_dc
V_ABSs, obj_fun_values, final_objective, epoch = PMMSGD.SGD_test_data(**params)

# +
fig = pplt.figure(refwidth='70mm', refheight = '55mm', sharex=True, sharey=False)
ax = fig.subplots(nrows=1, ncols=3)
s=4
ax[0].plot(np.arange(len(V_ABSs)), obj_fun_values, marker = '.', markersize=s)
ax[0].format(xlabel='iteration', ylabel='t-Delta')
ax[0].axhline(params['tolerance'], c='red')

ax[1].plot(np.arange(len(V_ABSs)), V_ABSs, marker = '.', markersize=s)
ax[1].axhline(-.622, c='red')
ax[1].axhline(params['V_ABS_min'], c='k')
ax[1].axhline(params['V_ABS_max'], c='k')
ax[1].format(ylabel='V_ABS')

ax[2].plot(np.array([PMMSGD.get_Delta_minus_t_test_data(V_ABS, params['classifier'], params['target']) for V_ABS in V_ABSs]), marker = '.', markersize=s)
ax[2].axhline(0, c='red')
ax[2].axhline(params['tolerance'], c='blue')
ax[2].axhline(-params['tolerance'], c='blue')
# -

PMMSGD.retrain_CNN(PMMSGD.cnn_model,
                   PMMSGD.cgan,
                PMMSGD.all_dbx[:5],
                ['lockinLL_G'],
                PMMSGD.all_Delta_minus_t[:5],
                center_x = None,
                center_y = None,
                epochs=5,
                batch_size=1,
                validation_split=0.1)

PMMSGD.retrain_CGAN(PMMSGD.cgan,
                    PMMSGD.all_dbx[:5],
                    ['lockinLL_G', 'lockinRR_G'],
                    PMMSGD.all_Delta_minus_t[:5],
                    x_points=25, center_x = None, center_y = None, epochs=5, batch_size=5)

# # Statistics

# +
params['verbose'] = False
V_ABS_dc = V_ABS_parameter("V_ABS", None, None)
params['V_ABS_parameter'] = V_ABS_dc

avgs, stds = [], []
learn_rates = np.logspace(-9, -5, 5)
for params['learning_rate'] in learn_rates:
    M=200
    epoch_ls =[]
    for _ in range(M):
        params['i_start'] = random.choice(range(len(PMMSGD.all_dbx)))
        V_ABSs, obj_fun_values, final_objective, epoch = PMMSGD.SGD_test_data(**params)
        epoch_ls.append(epoch)
    epoch_avg = np.mean(epoch_ls)
    avgs.append(epoch_avg)
    stds.append(np.std(epoch_ls))
    plt.scatter(params['learning_rate'], epoch_avg)

# -

plt.plot(learn_rates[:11], avgs)
plt.xscale('log')

# ## retraining

PMMSGD.retrain_CNN(PMMSGD.cnn_model,
                   PMMSGD.cgan,
                PMMSGD.all_dbx[:],
                ['lockinLL_G', 'lockinRR_G'],  # here I varied the inputs
                PMMSGD.all_Delta_minus_t[:],
                center_x = None,
                center_y = None,
                epochs=20,
                batch_size=4,
                validation_split=0.0)
