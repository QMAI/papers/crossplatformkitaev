# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.15.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
# %load_ext autoreload
# %autoreload 2
import random
import os
import xarray
import numpy as np
import matplotlib.pyplot as plt
# import proplot as pplt
import PMMSGD
from qcodes import Parameter, validators

class V_ABS_parameter(Parameter):
    def __init__(self, name, V_ABS_min, V_ABS_max):
        if V_ABS_min != None:
            vals = validators.Numbers(min_value=V_ABS_min, max_value=V_ABS_max)
            super().__init__(name=name, vals=vals)
        else:
            super().__init__(name=name)
        self.V_ABS = 0
    def get_raw(self):
        return self.V_ABS
    
    def set_raw(self, V_ABS):
        self.V_ABS = V_ABS


# -

# # Test function
# Mimics real measurement, uses cGAN to generate data

from skimage.transform import resize
def return_dbx():
    param = .5*(1e3*(V_ABSs_range[0]-V_ABS_dc())/V_ABSs_range.min()-.75)
    # GLL, DmtL = PMMSGD.cgan.generate_G_return(param+np.random.normal()*.2e-1, PMMSGD.scaler, PMMSGD.scaler_1, 1)
    # GRR, DmtL = PMMSGD.cgan.generate_G_return(param+np.random.normal()*.2e-1, PMMSGD.scaler, PMMSGD.scaler_1, 1)

    GLL, DmtL = PMMSGD.cgan.generate_G_return(param, PMMSGD.scaler, PMMSGD.scaler_1, 1)
    GRR, DmtL = PMMSGD.cgan.generate_G_return(param, PMMSGD.scaler, PMMSGD.scaler_1, 1)
    
    GLL -= GLL.min()
    GRR -= GRR.min()
    
    # GLL = resize(GLL, (51, 51))
    # GRR = resize(GRR, (51, 51))
    
    # GLL_enlarged, GRR_enlarged = np.zeros((51, 51)), np.zeros((51, 51))
    # start_row = (GLL_enlarged.shape[0] - GLL.shape[0]) // 2
    # start_col = (GLL_enlarged.shape[1] - GLL.shape[1]) // 2
    # GLL_enlarged[start_row:start_row+GLL.shape[0], start_col:start_col+GLL.shape[1]] = GLL
    # GRR_enlarged[start_row:start_row+GLL.shape[0], start_col:start_col+GLL.shape[1]] = GRR

    
    dims = ('QD1_plunger_fine_Vdc', 'QD2_plunger_fine_Vdc')
    coords = {'QD1_plunger_fine_Vdc': np.arange(GLL.shape[0]), 'QD2_plunger_fine_Vdc': np.arange(GLL.shape[1])}

    # Create data variables
    lockinLL_G = xarray.DataArray(GLL, coords=coords, dims=dims, name='lockinLL_G')
    lockinRR_G = xarray.DataArray(GRR, coords=coords, dims=dims, name='lockinRR_G')

    # Create the Dataset
    ds = xarray.Dataset({
        'lockinLL_G': lockinLL_G,
        'lockinRR_G': lockinRR_G
    })
    return ds


# # Test run

i_start = random.choice(range(len(PMMSGD.all_dbx)))
dbx_start = PMMSGD.all_dbx[i_start]
V_ABS_start  = PMMSGD.all_V_ABSs[i_start]
params = dict(
    dbx_start=dbx_start, 
    V_ABS_start=V_ABS_start,
    learning_rate = 1e-7, #1e-7 is good
    epochs = 50,
    min_step_size = 60e-6, #60 uV
    tolerance =.045,
    N = 5,  # Use the past 5 data points, for example,
    momentum = .5e-1,
    break_condition='minimum',
    V_ABS_min=-0.62431,
    V_ABS_max=-.62,
    verbose=True,
    classifier = 'CNN_differences',
    target=['lockinLL_G'],
    measure_func = return_dbx,
    plot=False
    )
# V_ABS_dc = V_ABS_parameter("V_ABS", params['V_ABS_min'], params['V_ABS_max'])
V_ABS_dc = V_ABS_parameter("V_ABS", None, None)
V_ABSs_range = np.linspace(-.622, -.62)
normalized_range = .5*(1e3*(V_ABSs_range[0]-V_ABSs_range)/V_ABSs_range.min()-.75)
V_ABS_start = -.624
V_ABS_dc(V_ABS_start)
params['dbx_start'] = return_dbx()
params['V_ABS_start'] = V_ABS_start
params['V_ABS_parameter'] = V_ABS_dc
V_ABSs, obj_fun_values, final_objective, epoch = PMMSGD.SGD(**params)

# +
fig = pplt.figure(refwidth='70mm', refheight = '55mm', sharex=True, sharey=False)
ax = fig.subplots(nrows=1, ncols=2)
s=4
ax[0].plot(np.arange(len(V_ABSs)), obj_fun_values, marker = '.', markersize=s)
ax[0].format(xlabel='iteration', ylabel='t-Delta')
ax[0].axhline(params['tolerance'], c='red')

ax[1].plot(np.arange(len(V_ABSs)), V_ABSs, marker = '.', markersize=s)
ax[1].axhline(-.622, c='red')
ax[1].axhline(params['V_ABS_min'], c='k')
ax[1].axhline(params['V_ABS_max'], c='k')
ax[1].format(ylabel='V_ABS')

# ax[2].plot(np.array([PMMSGD.get_Delta_minus_t(V_ABS) for V_ABS in V_ABSs]), marker = '.', markersize=s)
# ax[2].axhline(0, c='red')
# ax[2].axhline(params['tolerance'], c='blue')
# ax[2].axhline(-params['tolerance'], c='blue')
# -

PMMSGD.retrain_CGAN(PMMSGD.cgan, 
                    PMMSGD.all_dbx[:5], 
                    ['lockinLL_G', 'lockinRR_G'], 
                    PMMSGD.all_Delta_minus_t[:5], 
                    x_points=25, center_x = None, center_y = None, epochs=5, batch_size=4)

PMMSGD.retrain_CNN(PMMSGD.cnn_model,
                   PMMSGD.cgan,
                PMMSGD.all_dbx[:5], 
                ['lockinLL_G', 'lockinRR_G'], 
                PMMSGD.all_Delta_minus_t[:5], 
                center_x = None, 
                center_y = None, 
                epochs=5, 
                batch_size=32, 
                validation_split=0.)

1


xarray.load_dataset('a', backend='h5netcdf')

# %debug

# # Statistics

params['verbose'] = False
avgs, stds = [], []
learn_rates = np.logspace(-9, -5, 5)
for params['learning_rate'] in learn_rates:
    M=10
    epoch_ls =[]
    for _ in range(M):
        V_ABSs, obj_fun_values, final_objective, epoch = PMMSGD.SGD(**params)
        epoch_ls.append(epoch)
    epoch_avg = np.mean(epoch_ls)
    avgs.append(epoch_avg)
    stds.append(np.std(epoch_ls))
    

plt.plot(learn_rates[:11], avgs)
plt.xscale('log')


# # Retrain

# +
def get_prediction_CGAN(cgan, dbx, target, x_points=25, center_x = None, center_y = None):
    """
    If you don't specify centers, it uses Alberto's centering algo
    
    Parameters:
    - cgan: trained CGAN model
    - x_points: number of different evaluations of the discriminator to obtain the rating function
        
    Returns:
    - t-Delta prediction of the CGAN averaged over gLL and gRR individually
    
    """
    if type(target) != list:
        target = [target]
    
    tD_cGAN_LL = 0
    for t in target:
        target_array = get_dbx_target(dbx, t)
        
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_array)
        cut_image = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y, plot_images=False)
        cut_image_scale = cgan.scale_exp_data(cut_image) 
        tD_cGAN = cgan.return_Delta_t(spectra=cut_image_scale, label=np.zeros((4,1)), scaler_1=scaler_1, n_plot=0, plot_orig = False, x_points=x_points)
        tD_cGAN += (tD_cGAN_LL - 0.35 ) * 4.5
    pred = tD_cGAN/len(target)
    return pred

def retrain_CGAN(cgan, dbx, target, labels, x_points=25, center_x = None, center_y = None, epochs=5, batch_size=4):
    """
    (Re-) training of the cGAN with new input data and labels.
    
    Parameters:
    - cgan: trained CGAN model
    - data: normalized [0,1] differential conductance data (28x28 pixels), array dimension: [#samples, 28, 28, 1].
            cGAN is trained individually on GLL and (or) GRR                                                                                
    - labels: normalized [0,1] t-Delta ratio in form of a 1D array, array dimension: [#samples, 1]
    - epochs: number of training iterations through full data set
    - batch_size: number of samples given to the cGAN at the same time    
    """
    if type(target) != list:
        target = [target]
    
    tD_cGAN_LL = 0
    for t in target:
        target_array = get_dbx_target(dbx, t)
        
        if center_x == None or center_y == None:
            center_x, center_y = find_center_hyperbolae(target_array)
        cut_image = cgan.cut_image_center(target_array[:,:],center_x=center_x, center_y=center_y, plot_images=False)
        cut_image_scale = cgan.scale_exp_data(cut_image) 
        # tD_cGAN = cgan.return_Delta_t(spectra=cut_image_scale, label=np.zeros((4,1)), scaler_1=scaler_1, n_plot=0, plot_orig = False, x_points=x_points)
        
        cgan.train(cut_image_scale, labels, epochs=epochs, batch_size=batch_size)
       
    return None


# -

# # Experimental code

# +
class QD_center(Parameter):
    def __init__(self, name):

        vals = qc.validators.Numbers(min_value=-20e-3, max_value=20e-3)
        super().__init__(name=name, vals=vals)
        self.center=0
        
    def get_raw(self):
        return self.center
    
    def set_raw(self, center):
        self.center = center

def measure_avoided_crossing(QD1_range, QD2_range, delay = .25, N=28, centering=False, plot=False):
    """
    QD1 = LMG_fine
    """
    QD1_center, QD2_center = LMG_fine_center(), RMG_fine_center()
    run_id = PM3.measurements.sweep2d(dev, 'LMG_fine', QD1_center-QD1_range/2, QD1_center+QD1_range/2, N, delay,
                  'RMG_fine', QD2_center-QD2_range/2, QD2_center+QD2_range/2, N, delay,
                  dev.iL, dev.iR, dev.gLL, dev.gRR, dev.gRL, dev.gLR,
                  show_progress=True, use_threads=True)
    dbx = run_id[0].to_xarray_dataset()
    if centering:
        QD1_new_center, QD2_new_center = return_center(dbx, plot=plot)
        LMG_fine_center(QD1_new_center)
        RMG_fine_center(QD2_new_center)
        return dbx
    else:
        return dbx

def return_center(dbx, plot=True):
    """
    QD1 = LMG_fine
    """
    RMGF = dbx.RMG_fine_Vdc.values
    LMGF = dbx.LMG_fine_Vdc.values
    center_x, center_y = PMMSGD.find_center_hyperbolae(dbx.lockinRR_G.values)
    RMGFc_RR = (RMGF[-1] - RMGF[0]) * center_x / (len(RMGF) - 1) + RMGF[0] 
    LMGFc_RR = (LMGF[-1] - LMGF[0]) * center_y / (len(LMGF) - 1) + LMGF[0]
    if plot:
        plt.figure()
        dbx.lockinRR_G.plot()
        plt.scatter(LMGFc_RR, RMGFc_RR)
        plt.show()

    center_x, center_y = PMMSGD.find_center_hyperbolae(dbx.lockinLL_G.values)
    RMGFc_LL = (RMGF[-1] - RMGF[0]) * center_x / (len(RMGF) - 1) + RMGF[0] 
    LMGFc_LL = (LMGF[-1] - LMGF[0]) * center_y / (len(LMGF) - 1) + LMGF[0]
    if plot:
        plt.figure()
        dbx.lockinLL_G.plot()
        plt.scatter(LMGFc_LL, RMGFc_LL)
        plt.show()

    QD1_new_center = (LMGFc_RR + LMGFc_LL)/2
    QD2_new_center =  (RMGFc_RR + RMGFc_LL)/2
    return QD1_new_center, QD2_new_center

LMG_fine_center = QD_center("LMG_fine_center")
RMG_fine_center = QD_center("RMG_fine_center")
# -

dbx, QD1_center, QD2_center = measure_avoided_crossing(QD1_center, QD1_range, QD2_center, QD2_range,  delay = delay_time, N=resolution, centering=True, lorentzian=False)

# # make pdf summary

# +
import numpy as np
import matplotlib.pyplot as plt
from xarray import Dataset
import qcodes as qc
from matplotlib.backends.backend_pdf import PdfPages

def convert_to_xarray(run_id):
    # Get the dataset for the run_id
    db = qc.load_by_id(run_id)
    xr_ds = db.to_xarray_dataset()
    gate_dc = get_settings_by_db(xr_ds, False)
    PG_val = gate_dc['PG']
    Dmt = PMMSGD.get_Delta_minus_t(xr_ds, 'CNN_differences', ['lockinLL_G', 'lockinRR_G'])
    return xr_ds, PG_val, Dmt

def plot_and_save(run_ids):
    path, db_name = os.path.split(qc.config.current_config.core.db_location)
    filename = f"{db_name}_runs_{run_ids[0]}_to_{run_ids[-1]}.pdf"
    with PdfPages(filename) as pdf:
        for i in range(0, len(run_ids), 3):  # Process 3 run_ids at a time
            fig, axs = plt.subplots(3, 2, figsize=(12, 9))

            for j in range(3):
                if i + j >= len(run_ids):
                    break

                run_id = run_ids[i + j]
                xr_ds, PG_val, Dmt = convert_to_xarray(run_id)

                # Plot lockinLL_G
                if 'lockinLL_G' in xr_ds:
                    xr_ds['lockinLL_G'].plot(ax=axs[j, 0])
                    axs[j, 0].set_title(f'Run ID: {run_id} Delta-t: {Dmt:.2f} - lockinLL_G')

                # Plot lockinRR_G
                if 'lockinRR_G' in xr_ds:
                    xr_ds['lockinRR_G'].plot(ax=axs[j, 1])
                    axs[j, 1].set_title(f'PG: {PG_val}V - lockinRR_G')

            plt.tight_layout()
            pdf.savefig(fig)
            plt.close(fig)

# Usage
run_ids = range(14, 29)  # Replace with your desired range of run_ids
plot_and_save(run_ids)

