# Project: Cross-Platform Machine-Learning-Assisted Control of Minimal Kitaev Chains

Supplementary code for the publication "Cross-Platform Machine-Learning-Assisted Control of Minimal Kitaev Chains" by David van Driel et al.

For more details about the work and code see: insert arXiv submission here.


#### Abstract:
Contemporary quantum devices are reaching new limits in size and complexity, allowing for the
experimental exploration of emergent quantum modes. However, this increased complexity intro-
duces significant challenges in device tuning and control, for example in the experimental realization
of Kitaev chains, which can host Majorana zero modes. In this work, we introduce an automated
tuning algorithm for a minimal two-site Kitaev chain, achieving the emergence of a Majorana bound
state. Our method is based on a machine learning (ML) model trained on simulated data. Then
we retrain it on data from a two-dimensional electron gas (2DEG) realization of a minimal Kitaev
chain, and finally apply it to a nanowire-based two-site Kiteav chain device. Utilizing a convolutional
neural network, we predict tunneling and Cooper pair splitting rates from differential conductance
measurements, employing these predictions to adjust the electrochemical potential to a sweet spot.
The algorithm successfully converges to a sweet spot within ±1.5 mV in 67.6% of attempts and
within ±4.5 mV in 80.9% of cases, typically finding a sweet spot in 45 minutes. This advancement
not only facilitates automatic tuning of longer Kitaev chains for quantum information experiments
but also enhances cross-platform transfer learning in complex quantum systems.

#### Authors:
David van Driel 1, Rouven Koch 2, Vincent Sietses 1, Sebastiaan L. D. ten Haaf 1, Chun-Xiao Liu 1,
Francesco Zatelli 1, Bart Roovers 1, Alberto Bordin 1, Nick van Loo 1, Guanzhong Wang 1, Jan Cornelis Wolff 1,
Grzegorz P. Mazur 1, Tom Dvir 1, Sasa Gazibegovic 3, Ghada Badawy 3, Erik P. A. M. Bakkers 3,
Michael Wimmer 1, Srijit Goswami 1, Jose L. Lado 2, Leo P. Kouwenhoven 1, and Eliska Greplova 4

1 QuTech and Kavli Institute of NanoScience, Delft University of Technology, 2600 GA Delft, The Netherlands
2 Department of Applied Physics, Aalto University, 02150 Espoo, Finland
3 Department of Applied Physics, Eindhoven University of Technology, 5600 MB Eindhoven, The Netherlands
4 Kavli Institute of Nanoscience, Delft University of Technology, 2600 GA Delft, The Netherlands


### Folder description:
- Automated-tuning algorithm: Contains the code with tuning algorithm, CNN and cGAN (not used in this work), and the plotting and retraining scripts. Contains its own readme with further information.
- 2DEG_labelled_data: Contains the 74 labelled experimental charge stability diagrams (CSDs) to retrain the CNN model
- S matrix code: Contains the scripts that we used to model and generate the differential conductance and CSDs for the effective Hamiltonian of the minimal Kitaev chain.


#### For the requirements and versions of needed packages see "requirements.txt" and the "setup.py" is located in the folder "Automated-tuning algorithm" 


#### For more information contact Eliska Greplova (e.greplova@tudelft.nl)
