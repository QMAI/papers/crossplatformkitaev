import numpy as np
import numpy.linalg as LA

def _get_Deltas(Delta0, tLI=.97, tRI=.97):
    DeltaL = tLI**2 * Delta0
    DeltaR = tRI**2 * Delta0
    return DeltaL, DeltaR

def _get_u_v(mu, Delta):
    E0 = np.sqrt(mu**2 + Delta**2)
    u = np.sqrt(0.5 - 0.5 * mu / E0)
    v = np.sqrt(0.5 + 0.5 * mu / E0)
    return u, v

def _get_h_BdG(EL, ER, Gamma_even, Gamma_odd):
    H = np.array([
        [EL, Gamma_odd, 0, Gamma_even],
        [Gamma_odd, ER, -Gamma_even, 0],
        [0, -Gamma_even, -EL, -Gamma_odd],
        [Gamma_even, 0, -Gamma_odd, -ER]
    ])
    return H

def _get_heff_W_matrix(h_BdG, uL, vL, uR, vR, tLO, tRO):
    es, wfs = LA.eigh(h_BdG)
    W = np.zeros((4, 8), dtype=complex)
    h_eff = np.zeros((4, 4))
    
    for n in range(len(es)):
        h_eff[n, n] = es[n]
        
        a, b, c, d = wfs[0, n], wfs[1, n], wfs[2, n], wfs[3, n]
        W[n, 0] = tLO * (-c * vL).conj()
        W[n, 1] = tLO * (a * uL).conj()
        W[n, 2] = tRO * (-d * vR).conj()
        W[n, 3] = tRO * (b * uR).conj()
        
        W[n, 4] = -tLO * (-a * vL).conj()
        W[n, 5] = -tLO * (c * uL).conj()
        W[n, 6] = -tRO * (-b * vR).conj()
        W[n, 7] = -tRO * (d * uR).conj()
    
    return h_eff, W

def _get_S_matrix(omega, h_eff, W):
    W_dg = W.T.conj()
    GF = LA.inv(omega * np.identity(4) - h_eff + 0.5 * 1j * np.matmul(W, W_dg))
    S = np.identity(8) - 1j * np.matmul(W_dg, np.matmul(GF, W))
    return S

def _get_conductance(S):
    
    R_Le_Le = 0
    R_Lh_Le = 0
    R_Re_Re = 0
    R_Rh_Re = 0
    
    T_Re_Le = 0
    T_Rh_Le = 0
    T_Le_Re = 0
    T_Lh_Re = 0
    
    for j in [0, 1]:
        for i in [0, 1]:
            R_Le_Le += abs(S[j, i])**2
            R_Lh_Le += abs(S[j+4, i])**2
            R_Re_Re += abs(S[j+2, i+2])**2
            R_Rh_Re += abs(S[j+6, i+2])**2
            
            T_Re_Le += abs(S[j+2, i])**2
            T_Rh_Le += abs(S[j+6, i])**2
            T_Le_Re += abs(S[j, i+2])**2
            T_Lh_Re += abs(S[j+4, i+2])**2
            
    G_LL = 2 - R_Le_Le + R_Lh_Le
    G_RL = 0 - T_Re_Le + T_Rh_Le
    G_RR = 2 - R_Re_Re + R_Rh_Re
    G_LR = 0 - T_Le_Re + T_Lh_Re
    G_matrix = np.array([
        [G_LL, G_LR],
        [G_RL, G_RR]
    ])
    return G_matrix

def _get_GT(G0, omega_range, T):
    domega = omega_range[1] - omega_range[0]
    df_dE = (4 * T)**-1 * np.cosh(omega_range/(2*T))**-2
    GT = np.convolve(a=G0, v=df_dE, mode='same') * domega
    return GT

def _do_G(omega0, h_eff, W, T, omega_range):
    gLL0 = np.zeros(len(omega_range))
    gLR0 = np.zeros(len(omega_range))
    gRL0 = np.zeros(len(omega_range))
    gRR0 = np.zeros(len(omega_range))

    for k, omega in enumerate(omega_range):
        S_matrix = _get_S_matrix(omega=omega0+omega, h_eff=h_eff, W=W)
        G_matrix = _get_conductance(S=S_matrix)
        gLL0[k] = G_matrix[0, 0]
        gLR0[k] = G_matrix[0, 1]
        gRL0[k] = G_matrix[1, 0]
        gRR0[k] = G_matrix[1, 1]


    GLL = _get_GT(gLL0, omega_range, T=T)[4]
    GLR = _get_GT(gLR0, omega_range, T=T)[4]
    GRL = _get_GT(gRL0, omega_range, T=T)[4]
    GRR = _get_GT(gRR0, omega_range, T=T)[4]
    return GLL, GLR, GRL, GRR

def get_G(omega, Delta0, tLI, tRI, t_uu, t_dd, Delta_ud, Delta_du, t_ud, t_du, Delta_uu, Delta_dd,UL, UR, EzL, EzR ,muR, muL, T, Gamma_L, Gamma_R, omega_steps=10):
    """Calculates the full conductance matrix for a proximised DQD system.
    Input:
        omega
        Delta0
        tLI
        tRI
        t_uu
        t_dd
        Delta_ud
        Delta_du
        t_ud
        t_du
        Delta_uu
        Delta_dd
        UL
        UR
        EzL
        EzR
        muR
        muL
        T
        Gamma_L
        Gamma_R
        omega_steps = 10
    Returns:
        GLL, GLR, GRL, GRR
        """
    # Delta0, tLI, tRI, t_uu, t_dd, Delta_ud, Delta_du, t_ud, t_du, Delta_uu, Delta_dd,UL, UR, EzL, EzR ,muR, muL, T, Gamma_L, Gamma_R = itemgetter('Delta0', 'tLI', 'tRI', 
    #           't_uu', 't_dd', 'Delta_ud', 'Delta_du', 
    #           't_ud', 't_du', 'Delta_uu', 'Delta_dd',
    #           'UL' , 'UR',
    #           'EzL', 'EzR' ,
    #           'muR', 'muL', 'T',
    #           'Gamma_L', 'Gamma_R'
    #          )(params)
    
    omega_range = np.linspace(-3*T, 3*T, omega_steps)
    
    tLO, tRO = np.sqrt(Gamma_L), np.sqrt(Gamma_R)
    DeltaL, DeltaR = _get_Deltas(Delta0, tLI, tRI)
    
    uL, vL = _get_u_v(muL, DeltaL)
    uR, vR = _get_u_v(muR, DeltaR)

    Gamma_even = t_ud * vL * uR - t_du * uL * vR - Delta_uu * vL * vR + Delta_dd * uL * uR
    Gamma_odd = -t_uu * vL * vR + t_dd * uL * uR + Delta_ud * vL * uR - Delta_du * uL * vR

    EgL_even = -muL - np.sqrt(muL**2 + DeltaL**2)
    EgL_odd = -muL - UL/2 - EzL
    EL = EgL_odd - EgL_even

    EgR_even = -muR - np.sqrt(muR**2 + DeltaR**2)
    EgR_odd = -muR - UR/2 - EzR
    ER = EgR_odd - EgR_even

    h_BdG = _get_h_BdG(
        EL=EL,
        ER=ER,
        Gamma_even=Gamma_even,
        Gamma_odd=Gamma_odd
    )
    h_eff, W = _get_heff_W_matrix(
        h_BdG=h_BdG,
        uL=uL,
        vL=vL,
        uR=uR,
        vR=vR,
        tLO=tLO,
        tRO=tRO
    )

    GLL, GLR, GRL, GRR = _do_G(omega, h_eff, W, T, omega_range)
    return GLL, GLR, GRL, GRR, Gamma_even, Gamma_odd
    