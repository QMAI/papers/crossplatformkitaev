# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # %matplotlib widget
import matplotlib.pyplot as plt
from S_matrix_functions import *


def get_Deltas(Delta0, tLI=.97, tRI=.97, **unused_kwargs):
    DeltaL = tLI**2 * Delta0
    DeltaR = tRI**2 * Delta0
    return DeltaL, DeltaR


# +
params = dict(Delta0=.5, tLI=.97, tRI=.97, 
              t_uu=.2, t_dd=.2, Delta_ud=.2, Delta_du=.2, 
              t_ud=.15, t_du=.15, Delta_uu=-.2, Delta_dd=-.15,
              UL=10, UR = 10,
              EzL=0, EzR = 0,
              muR=0, muL=0,
              T=.05,
              Gamma_L=.05, Gamma_R=.05
             )

print(get_Deltas(**params))

# +
params = dict(Delta0=.5, tLI=.1, tRI=.1, 
              t_uu=.2, t_dd=.2, Delta_ud=.2, Delta_du=.2, 
              t_ud=.15, t_du=.15, Delta_uu=-.2, Delta_dd=-.15,
              UL=2, UR = 2,
              EzL=0, EzR = 0,
              muR=0, muL=0,
              T=.05,
              Gamma_L=.05, Gamma_R=.05
             )

print(get_Deltas(**params))

# +
params = dict(Delta0=.5, tLI=.1, tRI=.9, 
              t_uu=.15, t_dd=.23, Delta_ud=.1, Delta_du=.1, 
              t_ud=.25, t_du=.32, Delta_uu=-.4, Delta_dd=-.15,
              UL=5, UR = 4,
              EzL=0, EzR = 0,
              muR=0, muL=0,
              T=.05,
              Gamma_L=.05, Gamma_R=.05
             )

print(get_Deltas(**params))
# -

3*.95

# +
params = dict(Delta0=.5, tLI=.1, tRI=.9, 
              t_uu=2.85, t_dd=2.85, Delta_ud=0, Delta_du=0, 
              t_ud=.3, t_du=.3, Delta_uu=0, Delta_dd=0,
              UL=5, UR = 4,
              EzL=0, EzR = 0,
              muR=0, muL=0,
              T=.05,
              Gamma_L=.05, Gamma_R=.05
             )

print(get_Deltas(**params))

# +
params = dict(Delta0=.5, tLI=.35, tRI=.35, 
              t_uu=.2, t_dd=.2, Delta_ud=.2, Delta_du=.2, 
              t_ud=.2, t_du=.1, Delta_uu=-.2, Delta_dd=-.2,
              UL=2, UR = 2,
              EzL=0, EzR = 0,
              muR=0, muL=0,
              T=.01,
              Gamma_L=.05, Gamma_R=.05
             )

print(get_Deltas(**params))
# -

# ## CSD

muL_range = np.linspace(-2, 2, 100)
muR_range = np.linspace(-2, 2, 101)
GLLs = np.zeros((len(muL_range), len(muR_range)))
GRLs = np.zeros((len(muL_range), len(muR_range)))
GLRs = np.zeros((len(muL_range), len(muR_range)))
GRRs = np.zeros((len(muL_range), len(muR_range)))
params_c = params.copy()
for i in range(len(muL_range)):
    muL = muL_range[i]
    params_c['muL']=muL
    for j in range(len(muR_range)):
        muR = muR_range[j]
        params_c['muR']=muR
        
        GLL, GLR, GRL, GRR = get_G(0, **params_c)
        GLLs[i, j] = GLL
        GLRs[i, j] = GLR
        GRLs[i, j] = GRL
        GRRs[i, j] = GRR


# +
Glocal = np.max(GLLs)
Gnonlocal = np.max(abs(GLRs))
fig, axes = plt.subplots(2, 2, figsize=(7, 6))


ax = axes[0, 0]
im = ax.pcolor(muR_range, muL_range, GLLs, cmap='inferno_r', vmin=0, vmax=Glocal)
ax.set_xlabel(r'$\mu_R$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_L$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_LL [e^2/h]')
ax.text(-0.1, 1.1, '(a)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)

ax = axes[0, 1]
im = ax.pcolor(muR_range, muL_range, GLRs, cmap='bwr', vmin=-Gnonlocal, vmax=Gnonlocal)
ax.set_xlabel(r'$\mu_R$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_L$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_LR [e^2/h]')
ax.text(-0.1, 1.1, '(b)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)



ax = axes[1, 0]
im = ax.pcolor(muR_range, muL_range, GRLs, cmap='bwr', vmin=-Gnonlocal, vmax=Gnonlocal)
ax.set_xlabel(r'$\mu_R$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_L$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_RL [e^2/h]')
ax.text(-0.1, 1.1, '(a)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)

ax = axes[1, 1]
im = ax.pcolor(muR_range, muL_range, GRRs, cmap='inferno_r', vmin=0, vmax=Glocal)
ax.set_xlabel(r'$\mu_R$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_L$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_RR [e^2/h]')
ax.text(-0.1, 1.1, '(d)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)


plt.tight_layout()
plt.show()

# +
muR_range = np.linspace(1, 3, 100)
_muL = -2.3
plt.plot(muR_range, _muL*np.ones(len(muR_range)))


muR_range = np.linspace(1, 3, 100)
muL_range = np.linspace(-4, -1.2, 100)
plt.plot(muR_range, muL_range)
# -

# ## Spectrum: one dot

# +
omega_range = np.linspace(-1, 1, 110)
muR_range = np.linspace(0, 4, 100)
muL_range = np.linspace(-4, -1.2, 100)

GLLs = np.zeros((len(omega_range), len(muR_range)))
GRLs = np.zeros((len(omega_range), len(muR_range)))
GLRs = np.zeros((len(omega_range), len(muR_range)))
GRRs = np.zeros((len(omega_range), len(muR_range)))

params_c = params.copy()
params_c['muL'] = -2.3
for i, omega in enumerate(omega_range):
    omega = omega_range[i]
    for j, muR in enumerate(muR_range):
        muR = muR_range[j]
        params_c['muR']=muR
        # params_c['muL']=muL_range[j]
        
        GLL, GLR, GRL, GRR = get_G(omega, **params_c)
        GLLs[i, j] = GLL
        GLRs[i, j] = GLR
        GRLs[i, j] = GRL
        GRRs[i, j] = GRR


# +
Glocal = .4
Gnonlocal = 0.2
fig, axes = plt.subplots(2, 2, figsize=(7, 6))


ax = axes[0, 0]
im = ax.pcolor(muR_range, omega_range , GLLs, cmap='inferno_r', vmin=0, vmax=Glocal)
ax.set_xlabel(r'$\mu_R$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_L$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_LL [e^2/h]')
ax.text(-0.1, 1.1, '(a)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)

ax = axes[0, 1]
im = ax.pcolor(muR_range, omega_range , GLRs, cmap='RdBu_r', vmin=-Gnonlocal, vmax=Gnonlocal)
ax.set_xlabel(r'$\mu_R$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_L$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_LR [e^2/h]')
ax.text(-0.1, 1.1, '(b)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)



ax = axes[1, 0]
im = ax.pcolor(muR_range, omega_range , GRLs, cmap='RdBu_r', vmin=-Gnonlocal, vmax=Gnonlocal)
ax.set_xlabel(r'$\mu_R$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_L$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_RL [e^2/h]')
ax.text(-0.1, 1.1, '(a)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)

ax = axes[1, 1]
im = ax.pcolor(muR_range, omega_range , GRRs, cmap='inferno_r', vmin=0, vmax=Glocal)
ax.set_xlabel(r'$\mu_R$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_L$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_RR [e^2/h]')
ax.text(-0.1, 1.1, '(d)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)


plt.tight_layout()
# -

# ## Spectrum both dots

# +
omega_range = np.linspace(-1, 1, 110)
muR_range = np.linspace(1, 3, 100)
muL_range = np.linspace(-4, -1.2, 100)

GLLs = np.zeros((len(omega_range), len(muR_range)))
GRLs = np.zeros((len(omega_range), len(muR_range)))
GLRs = np.zeros((len(omega_range), len(muR_range)))
GRRs = np.zeros((len(omega_range), len(muR_range)))

params_c = params.copy()
params_c['muL'] = -1.95
for i, omega in enumerate(omega_range):
    omega = omega_range[i]
    for j, muR in enumerate(muR_range):
        muR = muR_range[j]
        params_c['muR']=muR
        params_c['muL']=muL_range[j]
        
        GLL, GLR, GRL, GRR = get_G(omega, **params_c)
        GLLs[i, j] = GLL
        GLRs[i, j] = GLR
        GRLs[i, j] = GRL
        GRRs[i, j] = GRR


# +
Glocal = .4
Gnonlocal = 0.2
fig, axes = plt.subplots(2, 2, figsize=(7, 6))


ax = axes[0, 0]
im = ax.pcolor(muR_range, omega_range , GLLs, cmap='inferno_r', vmin=0, vmax=Glocal)
ax.set_xlabel(r'$\mu_L$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_R$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_LL [e^2/h]')
ax.text(-0.1, 1.1, '(a)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)

ax = axes[0, 1]
im = ax.pcolor(muR_range, omega_range , GLRs, cmap='RdBu_r', vmin=-Gnonlocal, vmax=Gnonlocal)
ax.set_xlabel(r'$\mu_L$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_R$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_LR [e^2/h]')
ax.text(-0.1, 1.1, '(b)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)



ax = axes[1, 0]
im = ax.pcolor(muR_range, omega_range , GRLs, cmap='RdBu_r', vmin=-Gnonlocal, vmax=Gnonlocal)
ax.set_xlabel(r'$\mu_L$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_R$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_RL [e^2/h]')
ax.text(-0.1, 1.1, '(a)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)

ax = axes[1, 1]
im = ax.pcolor(muR_range, omega_range , GRRs, cmap='inferno_r', vmin=0, vmax=Glocal)
ax.set_xlabel(r'$\mu_L$', fontsize=12)
# ax.set_xticks([-1, 0, 1])
ax.set_ylabel(r'$\mu_R$', fontsize=12)
# ax.set_yticks([-1, 0, 1])
ax.set_title(r'G_RR [e^2/h]')
ax.text(-0.1, 1.1, '(d)', c='black', fontsize=12, transform=ax.transAxes)
plt.colorbar(im, ax=ax)


plt.tight_layout()
# -




